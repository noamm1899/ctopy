﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;


namespace CtoPy
{
    class CStat
    {
        public string code { get; set; }
        public CStat(string Code)
        {
            this.code = Code;
        }
        /// <summary>
        /// This funcs scans a c code and returns the precent of lines that were executed while running.
        /// </summary>
        /// <returns>Returns the precentage of lines that were executed.</returns>
        public double CoveragePrec()
        {
            string ins,
                counter;            
            int mainInd, //Find index of main function in code
                inBrack = 0, //Indicates whether the current index is inside a block.
                LnStrMain, // Finds the beggining of the line of the main function declaration.
                cnt = LinesCount(),  //Stores the original number of lines in the code (excluding lines outside of blocks).
                countFlg = 0, // Indicates which line the code is editing now.
                returnInd = -1,// Stores the index of the return function
                SreturnInd = -1, // Also stores the index of the return function. I need to save this twice.
                ind,
                LineStart;             
            counter = "\nint arr[" + cnt + "];\n";
            code = code.Insert(0, counter);
            SreturnInd = code.IndexOf("return ");
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] == '{')
                    inBrack++;
                if (code[i] == '}')
                    inBrack--;
                if (inBrack != 0 && i + 1 < code.Length && code[i] == ';' && code[i + 1] == '\n')
                {
                    LineStart = code.LastIndexOf('\n', i) + 1;
                    code = code.Insert(LineStart, "\narr[" + countFlg + "] ++;\n");
                    ins = "\narr[" + countFlg + "] ++;\n";
                    i += ins.Length;
                    countFlg++;
                }
            }
            mainInd = code.LastIndexOf(" main(");
            LnStrMain = code.LastIndexOf('\n', mainInd);
            code = code.Insert(LnStrMain, "\nvoid WriteToFile(int arr[]);\n");
            mainInd = EndOFunc(code.Substring(mainInd));
            returnInd = code.IndexOf("return ", mainInd);
            if (!isVoid(code.Substring(mainInd)))
                code = code.Insert(returnInd, "WriteToFile(arr);\n");
            else
            {
                mainInd = EndOFunc(code.Substring(mainInd));
                code = code.Insert(mainInd, "WriteToFile(arr);\n");
            }
            code = code.Insert(code.Length, "\nvoid WriteToFile(int arr[])\n{\nchar Sprec[50];\nFILE *f = fopen(\"log.txt\", \"w\");\nif (f == NULL)\n{\nprintf(\"Error opening file!\");\nexit(1);\n}	\nint count = 0;\nfor (int i = 0; i < " + cnt + " ; i++)\n{\nif (arr[i] == 1)\n{\ncount++;\n}\n}\nif (count == 0)\nfprintf(f, \"0\");\nelse\n{\ndouble prec = ((double)count / " + (double)cnt + ") * 100;\nsprintf(Sprec, \" % f\", prec);\nfprintf(f, Sprec);\n}\nfclose(f);\n}");
            mainInd = code.LastIndexOf(" main(");
            ind = code.IndexOf('{', mainInd);
            code = code.Insert(ind + 1, "\nfor(int i = 0; i <" + cnt + "; i++)\narr[i] = 0;\n");
            string path = @"CCode2.c";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(code);
                }
            }
            C c = new C(code);
            c.Run();
            double prec = 0;
            double.TryParse(System.IO.File.ReadAllText("log.txt"), out prec);
            return prec;
        } //need to do some touch ups, dont use yet.
        /// <summary>
        /// Returns the index of the end of the function.
        /// </summary>
        /// <param name="code">Code fron the begining of the function</param>
        /// <returns>Index of the end of the function.</returns>
        public int EndOFunc(string code)
        {
            int ind = 0;
            bool flag = false, changed = false;
            int counter = 0;
            for (int i = 0; i < code.Length && !flag; i++)
            {
                if (code[i] == '{')
                {
                    counter++;
                    changed = true;
                }
                if (code[i] == '}')
                    counter--;
                if (changed && counter == 0)
                {
                    ind = i;
                    flag = true;
                }       
            }
            return ind;
        }
        /// <summary>
        /// Checks if the given function should return a value.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool isVoid(string code)
        {
            int funcEnd = EndOFunc(code);
            string func = code.Substring(0, funcEnd);
            if (func.Contains("return "))
                return false;
            else
                return true;
        }
        /// <summary>
        /// Counts the number of lines in the code, Excluding lines that are not inside blocks.
        /// </summary>
        /// <returns>Number of lines</returns>
        public int LinesCount()
        {
            int count = 0, inBrack = 0;
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] == '{')
                    inBrack++;
                if (code[i] == '}')
                    inBrack--;
                if (inBrack != 0 && i + 1 < code.Length && code[i] == ';' && code[i + 1] == '\n')
                    count++;
            }
            return count;
        }

    }
}
