﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
namespace CtoPy
{
    class CCode:Block
    {
        public CCode(string code):base(null, code, 0)
        {
            Type = BlockType.Code;
        }
        public CCode(Block other):base(other)
        {
            Type = BlockType.Code;
        }
        public override string ToString()
        {
            return Code;
        }
        public new void SetBlockType(string type)
        {
        }
    }
}
