﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    /// <summary>
    /// The code inside a block (without the brackets)
    /// </summary>
    class Block
    {
        /// <summary>
        /// The line that opens the block
        /// </summary>
        public Line Opener { get; set; }
        /// <summary>
        /// The line that closes the block (only for do-while?)
        /// </summary>
        public Line Closer { get; set; }
        /// <summary>
        /// The code of the whole block
        /// </summary>
        private string _code;
        /// <summary>
        /// The whole code in text
        /// </summary>
        public string Code
        { get { return _code; } set { _code = value; SeperateBlocks(); } }
        /// <summary>
        /// Scope level of the code inside the block
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// The type of block, according to it's opener
        /// </summary>
        public BlockType Type { get; protected set; }
        /// <summary>
        /// An array of sub-blocks
        /// </summary>
        public Block[] Blocks { get; private set; }
        /// <summary>
        /// Array of lines that do not belong to any inner block
        /// </summary>
        public Line[] OuterLines { get; private set; }
        /// <summary>
        /// An array of lines of which the code consists
        /// </summary>
        public Line[] Lines
        {
            get
            {
                string[] splt = Split();
                List<Line> lines = new List<Line>();
                for (int i = 0; i < splt.Length; i++)
                    if (splt[i] != "")
                        lines.Add(new Line(splt[i], i));
                return lines.ToArray();
            }
        }

        public Block(Line opener, string code, int level)
        {
            Opener = opener;
            Code = code;
            Level = level;
            Closer = null;
            SeperateBlocks();
        }
        public Block(Line opener, Line closer, string code, int level)
        {
            Opener = opener;
            Code = code;
            Level = level;
            Closer = closer;
            SeperateBlocks();
        }
        public Block(Block other)
        {
            Opener = other.Opener;
            _code = other.Code;
            Closer = other.Closer;
            Level = other.Level;
            Blocks = other.Blocks;
            OuterLines = other.OuterLines;
            Type = other.Type;
        }

        /// <summary>
        /// Fills Blocks and OuterLines according to brackets
        /// </summary>
        protected virtual void SeperateBlocks()
        {
            int level = Level;
            Line[] lns = Lines;
            List<Block> blocks = new List<Block>();
            List<Line> outLines = new List<Line>();
            int openBracket = 0;
            bool doFlag = false;
            for (int i = 0; i < lns.Length; i++)
            {
                if (i + 1 < lns.Length && lns[i + 1].Code == "{")
                {
                    if (level++ == Level)
                    {
                        if (lns[i].Code == "do")
                            doFlag = true;
                        openBracket = ++i;
                    }
                }
                else if (lns[i].Code == "}")
                {
                    if (level == Level + 1)
                        if (doFlag)
                            blocks.Add(new Block(lns[openBracket - 1], lns[i + 1], GetLinesStr(openBracket + 1, i - 1), level));
                        else
                            blocks.Add(new Block(lns[openBracket - 1], GetLinesStr(openBracket + 1, i - 1), level));
                    level--;
                }
                else if (level == Level)
                    if (doFlag)
                        doFlag = false;
                    else
                    {
                        lns[i].Level = level;
                        outLines.Add(lns[i]);
                    }
            }
            Blocks = blocks.ToArray();
            if (Opener != null && Opener.Code.StartsWith("switch"))
                outLines.Clear();
            OuterLines = outLines.ToArray();
        }
        /// <summary>
        /// Sets code from array of lines
        /// </summary>
        /// <param name="lines">The lines in the code</param>
        public void SetText(string[] lines)
        {
            Code = string.Join("\n", lines);
        }
        public void SetText(Block[] blocks, Line[] outerlines)
        {
            if (Blocks.Length == blocks.Length && OuterLines.Length == outerlines.Length)
            {
                for (int i = 0; i < blocks.Length; i++)
                    Blocks[i].Type = blocks[i].Type;
                for (int i = 0; i < outerlines.Length; i++)
                    OuterLines[i].Type = outerlines[i].Type;
            }
        }
        /// <summary>
        /// Splits the code text by \n
        /// </summary>
        /// <returns>The lines in the code</returns>
        public string[] Split()
        {
            return Code.Split('\n');
        }
        /// <summary>
        /// Gets sub-array from array of lines
        /// </summary>
        /// <param name="from">first line index to get</param>
        /// <param name="to">last line index to get</param>
        /// <returns>sub-array of lines between given indexes</returns>
        protected Line[] GetLines(int from, int to)
        {
            return Lines.Skip(from).Take(to - from + 1).ToArray();
        }
        /// <summary>
        /// Gets string from array of lines
        /// </summary>
        /// <param name="from">first line index to get</param>
        /// <param name="to">last line index to get</param>
        /// <returns>sub-array of lines between given indexes as string</returns>
        protected string GetLinesStr(int from, int to)
        {
            string str = "";
            foreach (Line ln in GetLines(from, to))
                str += ln.Code.Substring(1) + '\n';
            return str;
        }
        public override string ToString()
        {
            Line[] lns = Lines;
            string str = (Opener != null ? Opener.Code : "") + "\n{\n";
            foreach (Line ln in lns)
                str += "\t" + ln.Code + "\n";
            str += "}";
            if (Closer != null)
                str += "\n" + Closer;
            return str;
        }
        public void SetBlockType(string type)
        {
            try
            {
                Type = (BlockType)Enum.Parse(typeof(BlockType), type);
            }
            catch (Exception)
            {
                Type = BlockType.Other;
            }
            if(Opener != null)
                Opener.Type = LineType.BlockLine;
            if (Closer != null)
                Closer.Type = LineType.BlockLine;
        }
    }
}
