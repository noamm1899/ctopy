﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CtoPy
{
    class Organizer
    {
        /// <summary>
        /// The source code, as received from user
        /// </summary>
        public CCode Src { get; set; }
        /// <summary>
        /// The code after the organization
        /// </summary>
        public CCode Dest { get; private set; }

        public List<string> StrApos { get; private set; }
        public List<string> CharApos { get; private set; }

        public Organizer(CCode cd)
        {
            Src = cd;
            Dest = new CCode(cd);
            StrApos = new List<string>();
            CharApos = new List<string>();
        }

        /// <summary>
        /// Starts the organizing operation
        /// </summary>
        public void Organize()
        {
            RemoveComments();
            DeleteEmptyLines();
            TrimNewlines();
            ReplaceApos();
            SplitByBracket();
            SplitBySemicolon();
            RemoveSpaceInBrack();
            SpaceCommas();
            BracketForCond();
            BracketsForCase();
            SplitDeclaration();
            Indent();
            SpaceOps();
            RemoveDeclarFromFor();
            SpaceInlineSemicolon();
            //RemoveSpaceInBrack();
            DeleteEmptyLines();
        }

        /// <summary>
        /// Indents the code correctly
        /// </summary>
        private void Indent()
        {
            string[] lines = Dest.Split();
            int count = 0;
            string line;
            for (int i = 0; i < lines.Length; i++)
            {
                line = lines[i].Trim('\t', ' ');
                if (line == "}")
                    count--;
                lines[i] = line.PadLeft(line.Length + count, '\t');
                if (line == "{")
                    count++;
            }
            Dest.SetText(lines);
        }
        /// <summary>
        /// Makes sure there is a space before and after the operators in the code
        /// </summary>
        private void SpaceOps()
        {
            string[] lines = Dest.Split();
            string line;
            string op = "";
            bool minusExcept = false;
            for (int i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (!line.StartsWith("#"))
                {
                    for (int j = 0; j < line.Length; j++)
                    {
                        if (GlobalConstants.Operators.Contains(line[j]))
                        {
                            op += line[j];
                            if (line[j] != '-')
                                minusExcept = true;
                        }
                        else if (op != "")
                        {
                            if (line[j - op.Length - 1] != ' ')
                                lines[i] = line = lines[i].Insert(j++ - op.Length, " ");
                            if (line[j] != ' ')
                                lines[i] = line = lines[i].Insert(j++ - (op[op.Length - 1] == '-' && minusExcept ? 1 : 0), " ");
                            op = "";
                            minusExcept = false;
                        }
                    }
                    op = "";
                }
            }
            Dest.SetText(lines);
        }
        /// <summary>
        /// This function removes all comments from the code.
        /// </summary>
        private void RemoveComments()
        {
            string code = Dest.Code;
            bool SingleQteFlag = false;
            bool DoubleQteFlag = false;
            //scanning the code for /* comments and removing them.
            bool longComm = code.Contains("/*");
            bool ShortComm = code.Contains("//");

            if (longComm || ShortComm)
            {
                for(int i=0; i < code.Length; i++)
                {
                    if (code[i] == '"' && (i == 0 || code[i - 1] != '\\') && !SingleQteFlag) //flag to check if comment is in quotation marks.
                        DoubleQteFlag = !DoubleQteFlag;
                    else if (code[i] == '\'' && (i == 0 || code[i - 1] != '\\') && !DoubleQteFlag) //flag to check if quote is inside quotes
                        SingleQteFlag = !SingleQteFlag;
                    else
                    {
                        if (i != (code.Length - 1) && code[i] == '/' && code[i + 1] == '*' && !DoubleQteFlag) //delete /* comments.
                            code = RemoveLongCmt(code, i);
                        else if (i != (code.Length - 1) && code[i] == '/' && code[i + 1] == '/' && !DoubleQteFlag) //delete // comments
                            code = RemoveShortCmt(code, i);
                    }
                }
                Dest.Code = code;
            }
        }
        /// <summary>
        /// This function adds a \n after each semicolon(;) if its not inside brackets or quotes;
        /// </summary>
        private void SplitBySemicolon()
        {
            string code = Dest.Code;
            bool BrkFlag = false;

            //scanning the code for /* comments and removing them.
            bool longComm = code.Contains(";");

            if (longComm)
            {
                for(int i =0; i <code.Length; i++)
                {
                    //chekcing if semicolon is inside a quotation marks or brackets.
                    if (code[i] == '(')
                        BrkFlag = true;
                    else if (code[i] == ')')
                        BrkFlag = false;
                    if (!BrkFlag && code[i] == ';' && (((i + 1) != code.Length) && (code[i + 1] != '\n')))
                        code = code.Insert(++i, "\n");
                }
                Dest.Code = code;
            }
        }
        /// <summary>
        /// Removes tabs and spaces before and after new lines
        /// </summary>
        private void TrimNewlines()
        {
            Dest.Code = Regex.Replace(Dest.Code, @"[ \t\r]*\n[ \t\r]*", "\n");
        }
        /// <summary>
        ///  This function adds a \n after each Bracket({ or }),
        ///  except for brackets that come after a '=' (arrays)
        /// </summary>
        private void SplitByBracket()
        {
            string code = Dest.Code;
            int inline = 0;

            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] == '{')
                {
                    if (code.Substring(i - 2, 2).Contains('=') || inline != 0)
                        inline++;
                    else
                    {
                        if (i + 1 == code.Length || code[i + 1] != '\n')
                            code = code.Insert(i + 1, "\n");
                        if (code[i - 1] != '\n')
                            code = code.Insert(i, "\n");
                    }
                }
                else if (code[i] == '}')
                {
                    if (inline != 0)
                        inline--;
                    else
                    {
                        if (i + 1 == code.Length || code[i + 1] != '\n')
                            code = code.Insert(i + 1, "\n");
                        if (code[i - 1] != '\n')
                            code = code.Insert(i, "\n"); 
                    }
                }
            }
            Dest.Code = code;
        }
        /// <summary>
        /// This function adds brackets for loops and condition if there are none.
        /// </summary>
        public void BracketForCond()
        {
            string[] BrackReq = { "for", "do", "while", "if", "else if", "else" };
            string code = Dest.Code;
            string currCode = "";
            int doCnt = 0;
            bool doBrackFlg = false;
            bool forFlg = false;
            int forCount = 0;
            int closerNum = 0;
            int indexOfAid = 0;
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] == ';')
                {
                    if (forFlg)
                    {
                        forCount++;
                        if (forCount == 2)
                        {
                            forCount = 0;
                            forFlg = false;
                        }
                    }
                    else if (closerNum > 0)
                    {
                        currCode = String.Concat(Enumerable.Repeat("\n}", closerNum));
                        currCode += "\n";
                        code = code.Insert(i + 1, currCode);
                        closerNum = 0;
                    }
                }
                else
                {
                    currCode = code.Substring(i);
                    foreach (string s in BrackReq)
                    {
                        if (currCode.StartsWith(s) && (i == 0 || !IsVarNameChar(code[i - 1])) && (i + s.Length >= code.Length || !IsVarNameChar(code[i + s.Length])))
                        {
                            switch(s)
                            {
                                case "else if":
                                    break;
                                case "else":
                                    indexOfAid = code.IndexOf('{', i);
                                    if (indexOfAid == -1 || !Regex.IsMatch(code.Substring(i + s.Length, indexOfAid - i - s.Length), "^\\s*$"))
                                    {
                                        code = code.Insert(i + 4, "\n{\n");
                                        closerNum++;
                                    }
                                    break;
                                case "if":
                                    goto default;
                                case "for":
                                    forFlg = true;
                                    goto default;
                                case "do":
                                    doCnt++;
                                    indexOfAid = code.IndexOf('{', i);
                                    if (indexOfAid == -1 || !Regex.IsMatch(code.Substring(i + s.Length, indexOfAid - i - s.Length), "^\\s*$"))
                                        code = code.Insert(i + 2, "\n{\n");
                                    else doBrackFlg = true;
                                    break;
                                case "while":
                                    indexOfAid = code.IndexOf(';', i);
                                    if (indexOfAid == -1 || doCnt > 0 && Regex.IsMatch(code.Substring(i + s.Length, indexOfAid - i - s.Length), "^\\s*\\(.*?\\)\\s*$"))
                                    {
                                        if (!doBrackFlg)
                                        {
                                            doCnt--;
                                            code = code.Insert(i, "\n}\n");
                                        }
                                    }
                                    else goto default;
                                    break;
                                default:
                                    indexOfAid = code.IndexOf('{', i);
                                    if (indexOfAid == -1 || !Regex.IsMatch(code.Substring(i + s.Length, indexOfAid - i - s.Length), "^\\s*\\(.*?\\)\\s*$"))
                                    {
                                        code = NewlineAfterBrk(code, i);
                                        closerNum++;
                                    }
                                    break;
                            }
                            break;
                        }
                    }
                }
            }
            Dest.Code = code;
        }
        /// <summary>
        /// Removes empty lines and double spaces from code
        /// </summary>
        private void DeleteEmptyLines()
        {
            Dest.Code = Regex.Replace(Dest.Code, @"\n\s*\n", "\n");
            Dest.Code = Regex.Replace(Dest.Code, @" +", " ");
        }
        /// <summary>
        /// This function seperates several declarations of the same varaiable type.
        /// </summary>
        private void SplitDeclaration()
        {
            Line[] ln = Dest.Lines;
            int codeBrackCount = 0;
            int lineBrackCount = 0;
            string ret = "";
            string[] vars = GlobalConstants.VarTypes;
            for (int i = 0; i < vars.Length; i++)
                vars[i] += " ";
            for (int i = 0; i < ln.Length; i++)
                if (ln[i].Code == "{")
                    codeBrackCount++;
                else if (ln[i].Code == "}")
                    codeBrackCount--;
                else if (codeBrackCount > 0 && ln[i].Code.Contains(",") && vars.Any(ln[i].Code.StartsWith))
                {
                    lineBrackCount = 0;
                    for (int j = 0; j < ln[i].Code.Length; j++)
                        if (ln[i].Code[j] == '(')
                            lineBrackCount++;
                        else if (ln[i].Code[j] == ')')
                            lineBrackCount--;
                        else if (lineBrackCount == 0 && ln[i].Code[j] == ',')
                        {
                            ln[i].Code = ln[i].Code.Remove(j, 1);
                            ln[i].Code = ln[i].Code.Insert(j, ";\n" + ln[i].Code.Split(' ')[0]);
                        }
                }
            foreach (Line line in ln)
                ret += line.ToString() + "\n";
            Dest.Code = ret;
        }
        /// <summary>
        /// Replacing "" and '' with id's and storing their contents in lists.
        /// </summary>
        private void ReplaceApos()
        {
            string code = Dest.Code,sub = "";
            int len;

            for(int i = 0; i != code.Length; i++)
            {
                if(code[i] == '\'')
                {
                    len = Regex.Match(code.Substring(i + 1), "(^|[^\\\\])'").Index + 3;
                    sub = code.Substring(i, len);
                    code = code.Remove(i, len);
                    code = code.Insert(i, "@" + CharApos.Count);
                    CharApos.Insert(CharApos.Count, sub);
                }
                else if(code[i] == '"')
                {
                    len = Regex.Match(code.Substring(i + 1), "(^|[^\\\\])\"").Index + 3;
                    sub = code.Substring(i, len);
                    code = code.Remove(i, len);
                    code = code.Insert(i, "$" + StrApos.Count);
                    StrApos.Insert(StrApos.Count, sub);
                }
            }
            Dest.Code = code;
        }
        /// <summary>
        /// This function removes unnecessary spaces from brackets.
        /// </summary>
        private void RemoveSpaceInBrack()
        {
            Dest.Code = Regex.Replace(Dest.Code, @"[ \t]*\([ \t]*", "(");
            Dest.Code = Regex.Replace(Dest.Code, @"[ \t]*\)[ \t]*", ")");
        }
        /// <summary>
        /// Adds brackets to cases in switch case operations.
        /// </summary>
        private void BracketsForCase()
        {
            Line[] lines = Dest.Lines, block;
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Code.StartsWith("switch(") || lines[i].Code.StartsWith("switch "))
                {
                    block = BrackForSwitchBlock(GetBlock(lines, i));
                    for(int j = 0; j < block.Length; j++)                    
                        lines[i + j].Code = block[j].Code;       
                }
            }
            string code = "";
            for (int i = 0; i < lines.Length; i++) 
                code += lines[i].Code + "\n";
            Dest.Code = code;

        }
        /// <summary>
        /// Removes variable declarations from For loops.
        /// </summary>
        private void RemoveDeclarFromFor()
        {
            string code = Dest.Code, var;
            string[] vars = GlobalConstants.VarTypes;
            int spaceInd, indx, formIndx = 0, len = Regex.Matches(code, @"for\(").Count;
            Match mtch = Regex.Match(code, @"for\(");

            for (int i = 0; i < len; i++)
            {
                indx = mtch.Index;
                spaceInd = code.IndexOf(" ", mtch.Index + formIndx);
                var = code.Substring(indx + 4 + formIndx, spaceInd - indx - formIndx - 4);
                if (vars.Contains(var))
                    code = code.Remove(indx + 4 + formIndx, spaceInd - indx - formIndx - 3);
                formIndx += mtch.Index + 4;
                mtch = Regex.Match(code.Substring(formIndx), "for\\(");
            }

            Dest.Code = code; 
        }
        /// <summary>
        /// Make sure there is exactly one space after comma
        /// </summary>
        private void SpaceCommas()
        {
            Dest.Code = Regex.Replace(Dest.Code, @",\s*", ", ");
        }
        /// <summary>
        /// Make sure there is exactly one space after inline semicolons
        /// </summary>
        private void SpaceInlineSemicolon()
        {
            Dest.Code = Regex.Replace(Dest.Code, @";(?=[^\s])", "; ");
            Dest.Code = Regex.Replace(Dest.Code, @";[^\S\r\n]+", "; ");
        }

        /// <summary>
        /// This function removes /* from code and is used inside RemoveComments().
        /// </summary>
        /// <param name="code"> code that needs to be changed.</param>
        /// <param name="idx">index of first /* in code</param>
        /// <returns>code without /* comments</returns>
        private string RemoveLongCmt(string code, int idx)
        {
            int EndInd = idx;
            bool flag = true;
            while (EndInd < code.Length && flag)
            {
                if ((EndInd != (code.Length - 1)) && (code[EndInd] == '*') && (code[EndInd + 1] == '/'))
                    flag = false;
                EndInd++;
            }
            code = code.Remove(idx, EndInd - idx + 1);
            return code;
        }
        /// <summary>
        /// This function removes // from code and is used inside RemoveComments().
        /// </summary>
        /// <param name="code">code that needs to be changed.</param>
        /// <param name="ind">index of first // in code</param>
        /// <returns>code without // comments</returns>
        private string RemoveShortCmt(string code, int ind)
        {
            int EndInd = ind;
            bool flag = true;
            while (EndInd != code.Length && flag)
            {
                if ((EndInd != (code.Length - 1)) && (code[EndInd] == '\n'))
                    flag = false;
                EndInd++;
            }
            code = code.Remove(ind, EndInd - ind - 1);
            return code;
        }
        /// <summary>
        /// This function finds the Nth occurance of a char in a string.
        /// </summary>
        /// <param name="s">String where the searching occures</param>
        /// <param name="t">Char to search for</param>
        /// <param name="n">WHich occurance to return</param>
        /// <returns></returns>
        private int GetNthIndex(string s, char t, int n)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == t)
                {
                    count++;
                    if (count == n)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        /// <summary>
        /// Add new line after closing bracket of opener at idx
        /// </summary>
        /// <param name="s">string to add newline on</param>
        /// <param name="idx">index of beginning of line to add newline after</param>
        /// <returns></returns>
        private string NewlineAfterBrk(string s, int idx)
        {
            int BrkCnt = 0;
            for (int i = idx; i < s.Length; i++)
            {
                if (s[i] == '(')
                    BrkCnt++;
                else if (s[i] == ')')
                {
                    if (BrkCnt == 1)
                    {
                        s = s.Insert(i + 1, "\n{\n");
                        break;
                    }
                    else BrkCnt--;
                }
            }
            return s;
        }
        /// <summary>
        /// Checks wether the char may belong to a var name
        /// </summary>
        /// <param name="c">char to check</param>
        /// <returns>true of char may belong to a var name</returns>
        private bool IsVarNameChar(char c)
        {
            return char.IsLetterOrDigit(c) || c == '_';
        }
        /// <summary>
        /// Fetch the lines of the next block
        /// </summary>
        /// <param name="lns">The lines of the whole code</param>
        /// <param name="start">Index of the block's opener line</param>
        /// <returns>Whole block, including opener and brackets (in lines)</returns>
        private Line[] GetBlock(Line[] lns, int start)
        {
            Line[] ret = null;
            int brackCount = -1;
            lns = lns.Skip(start).ToArray();
            for (int i = 0; ret == null && i < lns.Length; i++)
            {
                if (lns[i].Code.Contains("{"))
                    brackCount++;
                else if (lns[i].Code.Contains("}"))
                {
                    if (brackCount == 0)
                        ret = lns.Take(i + 1).ToArray();
                    else brackCount--;
                }
            }
            return ret;
        }
        /// <summary>
        /// Adds brackets to cases and defaults in switch blocks.
        /// </summary>
        /// <param name="ln">Switch block from GetBlock</param>
        /// <returns></returns>
        private Line[] BrackForSwitchBlock(Line[] ln)
        {
            int inBlock = -1;
            bool notFirst = false;
            for (int i = 0; i < ln.Length; i++)
            {
                if(ln[i].Code.Contains("{") )
                {
                    inBlock++;
                }
                else if (ln[i].Code.Contains("}"))
                {
                    inBlock--;
                }
                else if(inBlock == 0)
                {
                    if (ln[i].Code.StartsWith("case(") || ln[i].Code.StartsWith("case ") || ln[i].Code.StartsWith("default:") || ln[i].Code.StartsWith("default "))
                    {
                        ln[i].Code += "\n{";
                        if (notFirst)
                            ln[i].Code = "}\n" + ln[i].Code;
                        notFirst = true;
                    }
                }
            }
            ln[ln.Length - 1].Code += "\n}";
            return ln;
        }
    }
    
}
