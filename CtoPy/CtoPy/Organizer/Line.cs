﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CtoPy
{
    /// <summary>
    /// Represents a single line of code
    /// </summary>
    class Line
    {
        /// <summary>
        /// The text of the line
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// The number of the line
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// The type of the line
        /// </summary>
        public LineType Type { get; set; }
        /// <summary>
        /// Level of the block containing the line
        /// </summary>
        public int Level { get; set; }

        public Line(string code, int number)
        {
            Code = code;
            Number = number;
        }

        /// <summary>
        /// Removes paddings and double spaces from the code line
        /// </summary>
        /// <returns>The trimmed line</returns>
        public string Trim()
        {
            string trimmed = Code.Trim(' ', '\t', '\n');
            return (new Regex("\\s+")).Replace(trimmed, " ");
        }
        public override string ToString()
        {
            return Code;
        }
        public void SetLineType(string type)
        {
            try
            {
                Type = (LineType)Enum.Parse(typeof(LineType), type);
            }
            catch (Exception)
            {
                Type = LineType.Other;
            }
        }
    }
}
