﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace CtoPy
{
    /// <summary>
    /// A connection to the database
    /// </summary>
    class Connection
    {
        /// <summary>
        /// The connection to the access database
        /// </summary>
        public OleDbConnection Conn { get; } =
            new OleDbConnection(Properties.Settings.Default.DatabaseConnectionString);
        /// <summary>
        /// Opens the database connection stream
        /// </summary>
        public void Open()
        {
            if (IsClosed())
                Conn.Open();
        }
        /// <summary>
        /// Closes the database connection stream
        /// </summary>
        public void Close()
        {
            if (!IsClosed())
                Conn.Close();
        }
        /// <summary>
        /// Checks whether the database connection stream is closed
        /// </summary>
        /// <returns>True if the database connection stream is closed</returns>
        public bool IsClosed()
        {
            return Conn.State == ConnectionState.Closed;
        }
    }
}
