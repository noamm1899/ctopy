﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;

namespace CtoPy
{
    /// <summary>
    /// Database service for C to Python translation
    /// </summary>
    static class CtoPyService
    {
        private static DataTable DB = new DataTable("CtoPy");
        private static DataTable Props = new DataTable("CtoPyProp");
        /// <summary>
        /// Connection to the database
        /// </summary>
        private static Connection Conn = new Connection();
        public static void LoadDB()
        {
            OleDbCommand cmd1 = new OleDbCommand("SELECT * FROM CtoPy", Conn.Conn);
            OleDbCommand cmd2 = new OleDbCommand("SELECT * FROM CtoPyProp", Conn.Conn);
            OleDbDataAdapter odbda1 = new OleDbDataAdapter(cmd1);
            OleDbDataAdapter odbda2 = new OleDbDataAdapter(cmd2);
            try
            {
                Conn.Open();
                odbda1.Fill(DB);
                odbda2.Fill(Props);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Conn.Close();
            }
        }
        /// <summary>
        /// Gets a C format by it's ID
        /// </summary>
        /// <param name="id">The row's ID</param>
        /// <returns>The format</returns>
        public static string GetCByID(int id)
        {
            //OleDbCommand cmd = new OleDbCommand("SELECT C FROM CtoPy WHERE ID = @id", Conn.Conn);
            //cmd.Parameters.AddWithValue("id", id);
            //string str = "";
            //try
            //{
            //    Conn.Open();
            //    //Turns "\n"s and "\t"s in string to actual newlines and tabs
            //    str = ((string)cmd.ExecuteScalar()).Replace("\\n", "\n").Replace("\\t", "\t");
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}
            //finally
            //{
            //    Conn.Close();
            //}
            //return str;
            return DB.Select("ID = " + id)[0]["C"].ToString().Replace("\\n", "\n").Replace("\\t", "\t");
        }
        /// <summary>
        /// Gets a Python format by it's ID
        /// </summary>
        /// <param name="id">The row's ID</param>
        /// <returns>The format</returns>
        public static string GetPythonByID(int id)
        {
            //OleDbCommand cmd = new OleDbCommand("SELECT Python FROM CtoPy WHERE ID = @id", Conn.Conn);
            //cmd.Parameters.AddWithValue("id", id);
            //string str = "";
            //try
            //{
            //    Conn.Open();
            //    //Turns "\n"s and "\t"s in string to actual newlines and tabs
            //    str = cmd.ExecuteScalar().ToString().Replace("\\n", "\n").Replace("\\t", "\t");
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}
            //finally
            //{
            //    Conn.Close();
            //}
            //return str;
            return DB.Select("ID = " + id)[0]["Python"].ToString().Replace("\\n", "\n").Replace("\\t", "\t");
        }
        /// <summary>
        /// Gets IDs of all rows that match the given parse type
        /// </summary>
        /// <param name="type">Line/Block/Code</param>
        /// <returns>List of IDs</returns>
        public static List<int> GetIDsByParseType(string type)
        {
            //OleDbCommand cmd = new OleDbCommand("SELECT ID FROM CtoPy WHERE ParseType = @t ORDER BY ID ASC", Conn.Conn);
            //cmd.Parameters.AddWithValue("t", type);
            //List<int> listr = new List<int>();
            //OleDbDataReader odbdr;
            //try
            //{
            //    Conn.Open();
            //    odbdr = cmd.ExecuteReader();
            //    while (odbdr.Read())
            //        listr.Add(int.Parse(odbdr["ID"].ToString()));
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}
            //finally
            //{
            //    Conn.Close();
            //}
            //return listr;
            List<int> listr = new List<int>();
            DataRow[] drs = DB.Select("ParseType = '" + type + "'", "ID ASC");
            foreach (var item in drs)
                listr.Add(int.Parse(item["ID"].ToString()));
            return listr;
        }
        /// <summary>
        /// Gets IDs and format strings of all rows that match the given parse type
        /// </summary>
        /// <param name="type">Line/Block/Code</param>
        /// <returns>Dictionary of format strings by ID</returns>
        public static Dictionary<int, string> GetCsByParseType(string type)
        {
            //OleDbCommand cmd = new OleDbCommand("SELECT ID, C FROM CtoPy WHERE ParseType = @t ORDER BY ID ASC", Conn.Conn);
            //cmd.Parameters.AddWithValue("t", type);
            //Dictionary<int, string> listr = new Dictionary<int, string>();
            //OleDbDataReader odbdr;
            //try
            //{
            //    Conn.Open();
            //    odbdr = cmd.ExecuteReader();
            //    while (odbdr.Read())
            //        //Turns "\n"s and "\t"s in string to actual newlines and tabs
            //        listr.Add(int.Parse(odbdr["ID"].ToString()), (odbdr["C"].ToString()).Replace("\\n", "\n").Replace("\\t", "\t"));
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}
            //finally
            //{
            //    Conn.Close();
            //}
            //return listr;
            Dictionary<int, string> listr = new Dictionary<int, string>();
            DataRow[] drs = DB.Select("ParseType = '" + type + "'", "ID ASC");
            foreach (var item in drs)
                listr.Add(int.Parse(item["ID"].ToString()), item["C"].ToString().Replace("\\n", "\n").Replace("\\t", "\t"));
            return listr;
        }
        /// <summary>
        /// Gets the description of the line
        /// </summary>
        /// <param name="id">Line's ID</param>
        /// <returns>Description string</returns>
        public static string GetDescriptionByID(int id)
        {
            //OleDbCommand cmd = new OleDbCommand("SELECT Description FROM CtoPy WHERE ID = @id", Conn.Conn);
            //cmd.Parameters.AddWithValue("id", id);
            //string str = "";
            //try
            //{
            //    Conn.Open();
            //    str = (string)cmd.ExecuteScalar();
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}
            //finally
            //{
            //    Conn.Close();
            //}
            //return str;
            return DB.Select("ID = " + id)[0]["Description"].ToString();
        }
        /// <summary>
        /// Returns a special property according to its description
        /// </summary>
        /// <param name="desc">The description of the row</param>
        /// <returns>Pair of destination by source form of the property</returns>
        public static string GetPropByDescription(string desc)
        {
            DataRow[] drs = Props.Select("Description = '" + desc + "'");
            return drs[0]["Prop"].ToString().Replace("\\n", "\n").Replace("\\t","\t");
        }

        /* Description of the database's format:
            $ - Add to beginning of code
            ^ - Add to the end of the code
            [   If any of the above is found in a middle of the destination format,   ]
            [   whatever comes before it replaces the line/block and what comes after ]
            [   it is to be inserted at the beginning/the end                         ]
            @<number> - A parameter identifier. The code that was extracted from the
                parameter area at the source format should be inserted at the parameter
                area with the same index at the destination format.
                The parameters are stored in the parser's "FetchedValues" property.
            ~<code>~ - A repeatable area. The number of times that the area was repeated
                is stored in the parser's "RepeatAreaRepeats" property.
            |<code>| - An optional area. Whether the optional code exists or not
                is stored in the parser's "OptionalAreasExist" property.
            \ - Special character identifier. May be used if a special char in the format
                is written in the code as a regular char. Right now it is used only
                for the operator "||", which is represented as "\|\|"
            _ - A special "space" character, to be used in the source format when
                there are no non-space chars between special identifiers (ex: @0_@1)
            , - The next character may be ',', and if it isn't it may be a ')'.

            * Rows with ParseType "Code" will contain simple regex
            */
    }
}
