﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace CtoPy
{
    /// <summary>
    /// Interprets Python files
    /// </summary>
    class Python : Executor
    {
        protected override string ExecutorPath
        {
            get
            {
                return "python";
            }
        }
        protected override string CmdParams
        {
            get
            {
                return "";
            }
        }
        protected override string TextFileExtension
        {
            get
            {
                return ".py";
            }
        }
        protected override string ExecutableFileExtension
        {
            get
            {
                return ".py";
            }
        }
        public Python(string code):base(code, Language.Python)
        {
        }
        public Python(string code, Input[] inputs):base(code, Language.Python, inputs)
        {
        } 
        public override bool Run()
        {
            try
            {
                Process process = MakeProcess(ExecutorPath, CmdParams + GenericFileName + TextFileExtension);
                StreamWriter myStreamWriter = process.StandardInput;
                for (int i = 0; i < Inputs.Length; i++)
                {
                    myStreamWriter.Write(Inputs[i].Value);
                }
                FillOutputs(process.StandardOutput.ReadToEnd());
                FillErrors(process.StandardError.ReadToEnd(), ErrorType.RunTime);
                process.WaitForExit();
                process.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return Errors.Length == 0;
        }
        public override bool MakeFile()
        {
            try
            {
                File.Create(GenericFileName + TextFileExtension).Close();
                File.WriteAllText(GenericFileName + TextFileExtension, Code);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }
        public override bool GenerateInputs()
        {
            throw new NotImplementedException();
        }
    }
}
