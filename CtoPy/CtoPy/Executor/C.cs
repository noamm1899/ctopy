﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;

namespace CtoPy
{
    /// <summary>
    /// Executes C files
    /// </summary>
    class C:Executor
    {
        protected override string ExecutorPath
        {
            get
            {
                return "gcc";
            }
        }
        protected override string CmdParams
        {
            get
            {
                // -o file.exe file.c
                return "-o " + GenericFileName + ExecutableFileExtension + " " + GenericFileName + TextFileExtension;
            }
        }
        protected override string TextFileExtension
        {
            get
            {
                return ".c";
            }
        }
        protected override string ExecutableFileExtension
        {
            get
            {
                return ".exe";
            }
        }
        public C(string code):base(code, Language.C)
        {
        }
        public C(string code, Input[] inputs):base(code, Language.C, inputs)
        {
        }
        public override bool Run()
        {
            try
            {
                Process process = MakeProcess(GenericFileName + ExecutableFileExtension, "");
                
                StreamWriter myStreamWriter = process.StandardInput;
                for (int i = 0; i < Inputs.Length; i++)
                {
                    myStreamWriter.Write(Inputs[i].Value);
                }
                myStreamWriter.Close();
                FillOutputs(process.StandardOutput.ReadToEnd());
                FillErrors(process.StandardError.ReadToEnd(), ErrorType.RunTime);
                process.WaitForExit();
                process.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return Errors.Length == 0;
        }
        public override bool MakeFile()
        {
            try
            {
                File.Create(GenericFileName + TextFileExtension).Close();
                File.WriteAllText(GenericFileName + TextFileExtension, Code);
                Process process = MakeProcess(ExecutorPath, CmdParams);
                FillOutputs(process.StandardOutput.ReadToEnd());
                FillErrors(process.StandardError.ReadToEnd(), ErrorType.CompileTime);
                process.WaitForExit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return Errors.Length == 0;
        }
        public override bool GenerateInputs()
        {
            throw new NotImplementedException();
        }
    }
}
