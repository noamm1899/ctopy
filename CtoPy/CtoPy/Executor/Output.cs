﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    class Output:IO
    {
        public new OutputType Type
        { get { return (OutputType)base.Type; } private set { base.Type = value; } }
        public Output(int startidx, string call, OutputType type, string value):base(startidx, call, type, value)
        {
        }
    }
}
