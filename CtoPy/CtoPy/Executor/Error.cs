﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    class Error
    {
        public ErrorType Type { get; private set; }
        public int Line { get; private set; }
        public string Text { get; private set; }
        public Error(ErrorType type, int line, string text)
        {
            Type = type;
            Line = line;
            Text = text;
        }
    }
}
