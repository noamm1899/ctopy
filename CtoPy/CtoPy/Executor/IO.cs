﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    abstract class IO
    {
        public int StartIndex { get; protected set; }
        public string Call { get; protected set; }
        public Enum Type { get; protected set; }
        public virtual string Value { get; protected set; }
        public IO(int startidx, string call, Enum type, string value)
        {
            StartIndex = startidx;
            Call = call;
            Type = type;
            Value = value;
        }
    }
}
