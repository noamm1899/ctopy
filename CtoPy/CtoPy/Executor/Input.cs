﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    class Input:IO
    {
        public new string Value
        {
            get
            {
                return base.Value;
            }

            set
            {
                base.Value = value;
            }
        }
        public new InputType Type
        { get { return (InputType)base.Type; } private set { base.Type = value; } }
        public Input(int startidx, string call, InputType type):this(startidx, call, type, "")
        {
        }
        public Input(int startidx, string call, InputType type, string value):base(startidx, call, type, value)
        {
        }
    }
}
