﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;

namespace CtoPy
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            CtoPyService.LoadDB();
            Input[] ins = Tests.FauxGenerateInputs(new string[] { "3\n1111111111111111111111111111111111111111n" });
            Tests.BasicTranslationWholeTest(Tests.Strings[6], true);
            sw.Stop();
            Console.WriteLine("Time: " + sw.Elapsed);
        }
    }
}
