﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    /// <summary>
    /// The type of returned output
    /// </summary>
    enum OutputType { Console, File };
    /// <summary>
    /// The type of given input
    /// </summary>
    enum InputType { Console, File, DateTime };
    /// <summary>
    /// The executor's supported language
    /// </summary>
    enum Language { C, Python };
    /// <summary>
    /// The type of returned error
    /// </summary>
    enum ErrorType { RunTime, CompileTime };
    /// <summary>
    /// The types of lines
    /// </summary>
    enum LineType { Library, Define, VarAssign, Static, OpAssign, FuncDecl,
        ShortIf, Output, Input, Flush, SystemCall, Inc, Dec, Return,
        VarDecl, RandSeed, BlockLine, StrInit, StrDecl, Builtin, Other, FuncCall };
    /// <summary>
    /// The types of blocks
    /// </summary>
    enum BlockType { FuncDef, If, Elif, Else, For, While, Switch, Code, Other};
    enum CodeType { Begin, End, Convert, Type, Op, OpenBracket, CloseBracket,
        Semicolon, Rand, Input, Len, Builtin };
    class GlobalConstants
    {
        /// <summary>
        /// Types of variables
        /// </summary>
        public static readonly string[] VarTypes = { "int", "double", "float", "char", "long", "signed", "unsigned", "short" };
        /// <summary>
        /// Types of loops
        /// </summary>
        public static readonly string[] LoopTypes = { "while", "for", "do" };
        /// <summary>
        /// Types of conditions
        /// </summary>
        public static readonly string[] ConditionTypes = { "if", "else", "switch" };
        /// <summary>
        /// Chars that all the operators consist of
        /// </summary>
        public static readonly char[] Operators = { '+', '-', '*', '/', '%', '>', '<', '&', '|', '^', '~', '=', '?', ':', '!' };
        /* Other operators:
            "==", "!=", ">=", "<=", "&&", "||",
            "<<", ">>", "+=", "-=", "*=", "/=",
            "%=", "<<=", ">>=", "&=", "|=", "^="
        */
        /// <summary>
        /// All of the digits in a decimal base
        /// </summary>
        public static readonly string Digits = "1234567890";
    }
}
