﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CtoPy
{
    static class Tests
    {
        /// <summary>
        /// Tests communication with executors
        /// </summary>
        public static void ExecutorTest()
        {
            // Creates a connection to a C executor with basic C code
            C c = new C("#include<stdio.h>\nint main(){printf(\"This is a C program!\");return 0;}\n");
            // Creates a connection to a Python executor with basic Python code
            Python py = new Python("print 'This is a Python program!'");
            // Cool polymorphism!
            Executor[] exs = { c, py };
            // Executes both codes
            foreach (Executor ex in exs)
            {
                Console.WriteLine("\n{0}\n" + StrLine(), ex.Language.ToString());
                ex.MakeFile();
                ex.Run();
                // Print errors and outputs
                Console.WriteLine("Errors:");
                foreach (Error err in ex.Errors)
                    Console.WriteLine(err.Text);
                Console.WriteLine("Outputs:");
                foreach (Output op in ex.Outputs)
                    Console.WriteLine(op.Value);
            }
        }
        /// <summary>
        /// Tests code organization
        /// </summary>
        public static void OrganizerTest(string code)
        {
            CCode c = new CCode(code);
            Organizer o = new Organizer(c);
            o.Organize();
            Console.WriteLine(o.Dest);
        }
        /// <summary>
        /// Creates a line of n '-'s
        /// </summary>
        /// <param name="n">Number of '-'s</param>
        /// <returns>Created line</returns>
        public static string StrLine(int n = 10)
        {
            string s = "\n";
            for (int i = 0; i < n; i++)
                s += "-";
            return s + "\n";
        }
        /// <summary>
        /// Tests line parsing
        /// </summary>
        /// <param name="line">Line to parse</param>
        public static void ParseLineTest(string line)
        {
            Parser p = new Parser();
            Console.WriteLine(line);
            if (p.ParseLine(line))
            {
                // Prints the values parsed from the line
                Console.WriteLine("FV:");
                foreach (var item in p.FetchedValues)
                    Console.WriteLine(item.Key + ": " + item.Value);
                // Prints whether the optional areas in the format exist
                Console.WriteLine("\nOAE:");
                foreach (var item in p.OptionalAreasExist)
                    Console.WriteLine(item);
                // Prints whether the repeatable areas in the format exist
                Console.WriteLine("\nRAR:");
                foreach (var item in p.RepeatAreasRepeats)
                    Console.WriteLine(item);
                Console.ReadKey();
            }
            else
                Console.WriteLine("Parse Failed!");
        }
        /// <summary>
        /// Tests block parsing
        /// </summary>
        /// <param name="block">Block to parse</param>
        public static void ParseBlockTest(string block)
        {
            Parser p = new Parser();
            Console.WriteLine(block);
            if (p.ParseBlock(block))
            {
                // Prints the values parsed from the block
                Console.WriteLine("FV:");
                foreach (var item in p.FetchedValues)
                    Console.WriteLine(item.Key + ": " + item.Value);
                // Prints whether the optional areas in the format exist
                Console.WriteLine("\nOAE:");
                foreach (var item in p.OptionalAreasExist)
                    Console.WriteLine(item);
                // Prints whether the repeatable areas in the format exist
                Console.WriteLine("\nRAR:");
                foreach (var item in p.RepeatAreasRepeats)
                    Console.WriteLine(item);
                Console.ReadKey();
            }
            else
                Console.WriteLine("Parse Failed!");
        }
        /// <summary>
        /// Test block seperating by printing all blocks
        /// </summary>
        /// <param name="code">Code object to test seperation on</param>
        public static void SeperateBlocksTest(CCode code)
        {
            Line[] lines = code.OuterLines;
            Block[] blocks = code.Blocks;
            int lc = 0;
            int bc = 0;
            while (lc < lines.Length || bc < blocks.Length)
            {
                if (lc < lines.Length && (bc >= blocks.Length || lines[lc].Number < blocks[bc].Opener.Number))
                    Console.WriteLine(lines[lc++]);
                else
                    Console.WriteLine(blocks[bc++]);
            }
        }
        /// <summary>
        /// Test translation of organized code from C to Python
        /// </summary>
        /// <param name="code">Organized code to test translation on</param>
        public static void TranslatorTest(CCode code)
        {
            Translator t = new Translator(code, null, null);
            t.Translate();
            Console.WriteLine(t.Src);
            Console.WriteLine(StrLine());
            Console.WriteLine(t.Dest);
        }
        /// <summary>
        /// Test the code analyzer of C codes.
        /// </summary>
        /// <param name="code"></param>
        public static void CStatTest(CCode code)
        {
            CStat st = new CStat(code.Code);
            Console.WriteLine(st.CoveragePrec());           
        }
        public static void BasicTranslationWholeTest(string str, bool run)
        {
            BasicTranslationWholeTest(str, run, new Input[0]);
        }
        public static void BasicTranslationWholeTest(string str, bool run, Input[] inputs)
        {
            Console.WriteLine(str + Tests.StrLine());
            C c;
            Python py;
            if (inputs.Length > 0)
                c = new C(str, inputs);
            else
                c = new C(str);
            c.MakeFile();
            if (run)
            {
                c.Run();
                Console.WriteLine("C: Outputs:" + Tests.StrLine());
                foreach (var item in c.Outputs)
                    Console.WriteLine(item.Value);
                Console.WriteLine("C: Errors:" + Tests.StrLine());
                foreach (var item in c.Errors)
                    Console.WriteLine(item.Text); 
            }
            Organizer o = new Organizer(new CCode(str));
            o.Organize();
            Console.WriteLine("\n\nOrganizer: " + Tests.StrLine() + o.Dest);
            Translator t = new Translator(o.Dest, o.StrApos, o.CharApos);
            t.Translate();
            Console.WriteLine("\n\nTranslator: " + Tests.StrLine() + t.Dest);
            if (inputs.Length > 0)
                py = new Python(t.Dest, inputs);
            else
                py = new Python(t.Dest);
            py.MakeFile();
            if (run)
            {
                py.Run();
                Console.WriteLine("\n\nPython: Outputs:" + Tests.StrLine());
                foreach (var item in py.Outputs)
                    Console.WriteLine(item.Value);
                Console.WriteLine("Python: Errors:" + Tests.StrLine());
                foreach (var item in py.Errors)
                    Console.WriteLine(item.Text); 
            }
        }
        public static Input[] FauxGenerateInputs(string[] inputs)
        {
            Input[] ret = new Input[inputs.Length];
            for (int i = 0; i < inputs.Length; i++)
                ret[i] = new Input(0, "", InputType.Console, inputs[i]);
            return ret;
        }

        public static readonly string[] Strings = {
                                        /*0 - Print nums loop*/"#include <stdio.h>\n#include <stdlib.h>\n\nint main()\n{\n\tfor(int i = 0; i < 5; i++)\n\t\tprintf(\"  %d  \", i);\n}",
                                        /*1 - Divisors of num*/"#include <stdio.h>\n#include <stdlib.h>\n\nint main()\n{\n\tint i=0;\n\tfor (i = 0; i < 100; i++)\n\t\tif(IsPrime(i))\n\t\t\tprintf(\"%d \",i);\n}\n\nint IsPrime(int number) {\n\tint i = 0;\n\tfor (i = 2; i < number; i++) {\n\t\tif (number % i == 0 && i != number) return 0;\n\t}\n\treturn 1;\n}",
                                        /*2 - Reverse num*/"#include<stdio.h>\nint main()\n{\nint num, temp, unit, rev=0;\nprintf(\"Enter a number:\\n\");\nnum=4321;\ntemp=num;\nfor(;temp!=0;temp/=10)\n{\nunit=temp%10;\nrev=rev*10+unit;\n}\nprintf(\"Original number: %d\\n\",num);\nprintf(\"Reversed number: %d\\n\",rev);\nsystem(\"pause\");\nreturn (0);\n}\n",
                                        /*3 - I/O format*/"#include <stdio.h>\n\nint main()\n{\n\tint a = 0;\n\tint b, c;\n\tscanf(\"%d\", &a);\n\tscanf(\"%d %d\", &b, &c);\n\tprintf(\"%d.%d.%d\",a,b,c);\n\treturn 0;\n}",
                                        /*4 - I/O functions*/"#include <stdio.h>\nint main()\n{\n\tprintf(\"Enter char without enter after:\");\n\tchar a = getch();\n\tprintf(\"Enter char with enter after:\");\n\tchar b = getchar();\n\tprintf(\"%c, %c\", a, b);\n\tsystem(\"pause\");\n\treturn 0;\n}",
                                        /*5 - Long proj w/o inputs*/"#include<stdio.h>\n#include<stdlib.h>\n#include<time.h>\nint randCode();\nvoid codeBreaker(int code);\nint guessFunc();\nint check(int guess, int code);\nint main()\n{ \n\tsrand(time(NULL));//makes all rand functions give different values every time.\n\tchar play=\'y\';\n\twhile(play==\'y\')//the game can be played endlessly, unless n is chosen when asked to play again. \n\t{\n\t\tprintf(\"\\nWelcome to \\\"MAGSHIMIM CODE-BREAKER\\\"!!!\\n\\n\");//introduction & instructions.\n\t\tprintf(\"A secret password was chosen to protect the credit card of Pancratius,\\n\");\n\t\tprintf(\"the descendant of Antiochus.\\n\");\n\t\tprintf(\"Your mission is to stop Pancratius by revealing his secret password.\\n\\n\");\n\t\tprintf(\"The rules are as follows:\\n\");\n\t\tprintf(\"1. In each round you try to guess the secret password (4 distinct digits)\\n\");\n\t\tprintf(\"2. After every guess you\'ll receive two hints about the password\\n\");\n\t\tprintf(\"   HITS:   The number of digits in your guess which were exactly right.\\n\");\n\t\tprintf(\"   MISSES: The number of digits in your guess which belongs to\\n\");\n\t\tprintf(\"           the password but were miss-placed.\\n\");\n\t\tprintf(\"3. If you\'ll fail to guess the password after a certain number of rounds\\n\");\n\t\tprintf(\"   Pancratius will buy all the gifts for Hanukkah!!!\\n\");\n\t\tint code = randCode();\n\t\tcodeBreaker(code);\n\t\tdo\n\t\t{\n\t\t\tprintf(\"\\nDo you want to try again? (y/n - no Enter is needed)\\n\");\n\t\t\tplay=\'n\';\n\t\t\tswitch (play)//if y-play again, if n-exit.\n\t\t\t{\n\t\t\t\tcase \'y\':\n\t\t\t\tbreak;\n\t\t\t\t\n\t\t\t\tcase \'n\':\n\t\t\t\tsystem(\"exit\");\n\t\t\t\tbreak;\n\t\t\t\t\n\t\t\t\tdefault:\n\t\t\t\tprintf(\"invalid option %c\\n\",play);\n\t\t\t\tbreak;\n\t\t\t}\n\t\t}\n\t\twhile(play!=\'y\'&&play!=\'n\');//asks again if input is invalid\n\t}\n\treturn(0);\n}\n/**\nChooses a random number, which is the code that needs to be guessed.\nThe code is made of 4 digits, each is between 1-6.\nNo digit is repeated twice.\n\nInput: none\nOutput: four-digit code.\n*/\nint randCode()\n{\n\tint units, tens, hunds, thous, code;\n\tdo\n\t{\n\t\t//randomizes digits, all between 1-6.\n\t\tunits=rand()%6+1;//rightmost digit\n\t\ttens=(rand()%6+1);//second to the right\n\t\thunds=(rand()%6+1);//second to the left\n\t\tthous=(rand()%6+1);//leftmost\n\t}\n\twhile((units==tens)||(units==hunds)||(units==thous)||(tens==hunds)||(tens==thous)||(hunds==thous));//make sure no digit equals another\n\tcode=units+tens*10+hunds*100+thous*1000;//connect to a single four-digit number\n\treturn code;\n}\n/**\nConnects most parts of the game.\nFirst it gets a number between 1-4, referring the level.\nThen it calls guessFunc and check functions.\nIt was made a function because most of it is related to the number of rounds.\n\nInput: The random code from randCode function.\nOutput:none (void)\n*/\nvoid codeBreaker(int code)\t\n{\n\tint level; //difficulty of the game\n\tint round=0, elapsedRound=0; //round is the chosen number of rounds, while elapsedRound is the number of rounds passed during the game.\n\tint ans=0; //feedback; check if something is true (because we didn\'t learn bool yet!!)\n\tdo\n\t{\n\t\tprintf(\"\\nChoose the game level:\\n\");\n\t\tprintf(\"1 - Easy (20 rounds)\\n\");\n\t\tprintf(\"2 - Moderate (15 rounds)\\n\");\n\t\tprintf(\"3 - Hard (10 rounds)\\n\");\n\t\tprintf(\"4 - Crazy (random number of rounds 5-25)\\n\");\n\t\tprintf(\"Make a choice: \");\n\t\tlevel = 3;\n\t\t_flushall();\n\t\tswitch(level)\n\t\t{\n\t\t\tcase 1:\n\t\t\tround=20;\n\t\t\tbreak;\n\t\t\t\n\t\t\tcase 2:\n\t\t\tround=15;\n\t\t\tbreak;\n\t\t\t\n\t\t\tcase 3:\n\t\t\tround=10;\n\t\t\tbreak;\n\t\t\t\n\t\t\tcase 4:\n\t\t\tround=rand()%21+5;//random number between 5-25\n\t\t\tbreak;\n\t\t\t\n\t\t\tdefault:\n\t\t\tprintf(\"invalid option: %d\\n\",level);\n\t\t\tbreak;\n\t\t}\n\t}\n\twhile(level<1||level>4);//repeats if invalid input\n\tdo\n\t{\n\t\tint guess=guessFunc();//calls guessFunc function and puts returned value into guess.\n\t\tans=check(guess,code);//sends guess and randomized code into check function, places return into ans (if answer is right then ans=1)\n\t\tround--;//decrease 1 from round each time, so the loop will repeat \'round\' times.\n\t\telapsedRound++;//stores the number of rounds elapsed by now.\t\n\t}\n\twhile(round!=0&&ans==0);//if user is out of rounds or if ans returns true, loop ends.\n\tif(ans==1)//checks if loop ended because user is out of rounds or because he guessed right and ans is true.\n\t{\n\t\tprintf(\"\\nYou won!!!\\n\");\n\t\tprintf(\"It took you only %d guesses, you are a professional code breaker!\\n\",elapsedRound);\n\t}\n\telse\n\t{\n\t\tprintf(\"\\nOOOOHHHH!!! Pancratius won and bought all of Hanukkah\'s gifts.\\n\");\n\t\tprintf(\"Nothing left for you...\\n\");\n\t\tprintf(\"The secret password was %d\\n\",code);\n\t}\t\n}\n/**\nGets 4 digits between 1-6 from the user. This is his guess.\n\nInput: none\nOutput: whole guess, as four-digit integer.\n*/\nint guessFunc()\n{\n\tint units, tens, hunds, thous, guess;\n\tprintf(\"\\nWrite your guess (only 1-6, no ENTER is needed)\\n\");\n\tdo\n\t{\n\t\tthous=\'1\'-48;\n\t}\n\twhile(thous<1||thous>6);//repeats if input is not between 1-6. leftmost digit.\n\tprintf(\"%d\",thous);//only prints after loop - if input is between 1-6.\n\tdo\n\t{\n\t\thunds=\'2\'-48;\n\t}\n\twhile(hunds<1||hunds>6);//second to the left\n\tprintf(\"%d\",hunds);\n\tdo\n\t{\n\t\ttens=\'3\'-48;\n\t}\n\twhile(tens<1||tens>6);//second to the right\n\tprintf(\"%d\",tens);\n\tdo\n\t{\n\t\tunits=\'4\'-48;\n\t}\n\twhile(units<1||units>6);//rightmost\n\tprintf(\"%d\\n\",units);\n\tguess=units+tens*10+hunds*100+thous*1000;//connects digits into a 4-digit integer.\n\treturn guess;\n}\n/**\nChecks how many digits missed and how many hit,\nby comparing all digits between randomized code and the user\'s guess this turn.\n\nInput: randomized code from randCode, user\'s guess from guessFunc.\nOutput: ans, \'bool\' if the whole guess was true, else false.\n*/\nint check(int guess, int code)\n{\t\n\tint tempC;//temporary code: copies code\'s value so it wouldn\'t be changed.\n\tint digitG, digitC;//digit guess, digit code: the rightmost digit of current value of each.\n\tint miss=0, hit=0;//count hits and misses for each round\n\tint i, j;//for the for loops. symbolizes the place of the digit in each number (i-digitG, j-digitC)\n\tint ans=0;//bool, as mentioned above.\n\tfor(i=1;i<=4;i++)//repeats four times-once for each digit of the guess.\n\t{\n\t\tdigitG=guess%10;//sets rightmost digit of guess as digitG\n\t\ttempC=code;//copies code into tempC, so code would stay as is, and so it would always start from the same number in the current game.\n\t\tfor(j=1;j<=4;j++)//repeats four times-once for each digit of the random code.\n\t\t{\n\t\t\tdigitC=tempC%10;//sets rightmost digit of code as digitC\n\t\t\tif(digitC==digitG)//checks if current digits of code and guess equal. if true, then there\'s a miss or a hit.\n\t\t\t{\n\t\t\t\tif(i==j)//checks if the place of the digits is equal. if true, then it\'s a hit.\n\t\t\t\t{\n\t\t\t\t\thit++;\n\t\t\t\t}\n\t\t\t\telse//if place of the digits is not equal, then it\'s a miss.\n\t\t\t\t{\n\t\t\t\t\tmiss++;\n\t\t\t\t}\n\t\t\t}\n\t\t\ttempC/=10;//divides tempC by 10, to move on to the next digit (to the right) of the random code.\n\t\t}\n\t\tguess/=10;//divides guess by 10, to move on to the next digit (to the right) of the user\'s guess.\n\t}\n\tprintf(\"You got %*d HITS %*d MISSES.\\n\",5,hit,5,miss);//print number of hits and/or misses.\n\tif (hit==4)//if there are 4 hits, it means the user\'s guess is right and then ans is returned true, and the current session is finished.\n\t{\n\t\tans=1;\n\t}\n\treturn ans;\n}\n",
                                        /*6 - Long proj w. inputs*/"#include<stdio.h>\n#include<stdlib.h>\n#include<time.h>\nint randCode();\nvoid codeBreaker(int code);\nint guessFunc();\nint check(int guess, int code);\nint main()\n{ \n\tsrand(time(NULL));//makes all rand functions give different values every time.\n\tchar play=\'y\';\n\twhile(play==\'y\')//the game can be played endlessly, unless n is chosen when asked to play again. \n\t{\n\t\tprintf(\"\\nWelcome to \"\"MAGSHIMIM CODE-BREAKER\"\"!!!\\n\\n\");//introduction & instructions.\n\t\tprintf(\"A secret password was chosen to protect the credit card of Pancratius,\\n\");\n\t\tprintf(\"the descendant of Antiochus.\\n\");\n\t\tprintf(\"Your mission is to stop Pancratius by revealing his secret password.\\n\\n\");\n\t\tprintf(\"The rules are as follows:\\n\");\n\t\tprintf(\"1. In each round you try to guess the secret password (4 distinct digits)\\n\");\n\t\tprintf(\"2. After every guess you\'ll receive two hints about the password\\n\");\n\t\tprintf(\"   HITS:   The number of digits in your guess which were exactly right.\\n\");\n\t\tprintf(\"   MISSES: The number of digits in your guess which belongs to\\n\");\n\t\tprintf(\"           the password but were miss-placed.\\n\");\n\t\tprintf(\"3. If you\'ll fail to guess the password after a certain number of rounds\\n\");\n\t\tprintf(\"   Pancratius will buy all the gifts for Hanukkah!!!\\n\");\n\t\tint code = randCode();\n\t\tcodeBreaker(code);\n\t\tdo\n\t\t{\n\t\t\tprintf(\"\\nDo you want to try again? (y/n - no Enter is needed)\\n\");\n\t\t\tplay=getch();\n\t\t\tswitch (play)//if y-play again, if n-exit.\n\t\t\t{\n\t\t\t\tcase \'y\':\n\t\t\t\tbreak;\n\t\t\t\t\n\t\t\t\tcase \'n\':\n\t\t\t\tsystem(\"exit\");\n\t\t\t\tbreak;\n\t\t\t\t\n\t\t\t\tdefault:\n\t\t\t\tprintf(\"invalid option %c\\n\",play);\n\t\t\t\tbreak;\n\t\t\t}\n\t\t}\n\t\twhile(play!=\'y\'&&play!=\'n\');//asks again if input is invalid\n\t}\n\treturn(0);\n}\n/**\nChooses a random number, which is the code that needs to be guessed.\nThe code is made of 4 digits, each is between 1-6.\nNo digit is repeated twice.\n\nInput: none\nOutput: four-digit code.\n*/\nint randCode()\n{\n\tint units, tens, hunds, thous, code;\n\tdo\n\t{\n\t\t//randomizes digits, all between 1-6.\n\t\tunits=rand()%6+1;//rightmost digit\n\t\ttens=(rand()%6+1);//second to the right\n\t\thunds=(rand()%6+1);//second to the left\n\t\tthous=(rand()%6+1);//leftmost\n\t}\n\twhile((units==tens)||(units==hunds)||(units==thous)||(tens==hunds)||(tens==thous)||(hunds==thous));//make sure no digit equals another\n\tcode=units+tens*10+hunds*100+thous*1000;//connect to a single four-digit number\n\treturn code;\n}\n/**\nConnects most parts of the game.\nFirst it gets a number between 1-4, referring the level.\nThen it calls guessFunc and check functions.\nIt was made a function because most of it is related to the number of rounds.\n\nInput: The random code from randCode function.\nOutput:none (void)\n*/\nvoid codeBreaker(int code)\t\n{\n\tint level; //difficulty of the game\n\tint round=0, elapsedRound=0; //round is the chosen number of rounds, while elapsedRound is the number of rounds passed during the game.\n\tint ans=0; //feedback; check if something is true (because we didn\'t learn bool yet!!)\n\tdo\n\t{\n\t\tprintf(\"\\nChoose the game level:\\n\");\n\t\tprintf(\"1 - Easy (20 rounds)\\n\");\n\t\tprintf(\"2 - Moderate (15 rounds)\\n\");\n\t\tprintf(\"3 - Hard (10 rounds)\\n\");\n\t\tprintf(\"4 - Crazy (random number of rounds 5-25)\\n\");\n\t\tprintf(\"Make a choice: \");\n\t\tscanf(\"%d\",&level);\n\t\t_flushall();\n\t\tswitch(level)\n\t\t{\n\t\t\tcase 1:\n\t\t\tround=20;\n\t\t\tbreak;\n\t\t\t\n\t\t\tcase 2:\n\t\t\tround=15;\n\t\t\tbreak;\n\t\t\t\n\t\t\tcase 3:\n\t\t\tround=10;\n\t\t\tbreak;\n\t\t\t\n\t\t\tcase 4:\n\t\t\tround=rand()%21+5;//random number between 5-25\n\t\t\tbreak;\n\t\t\t\n\t\t\tdefault:\n\t\t\tprintf(\"invalid option: %d\\n\",level);\n\t\t\tbreak;\n\t\t}\n\t}\n\twhile(level<1||level>4);//repeats if invalid input\n\tdo\n\t{\n\t\tint guess=guessFunc();//calls guessFunc function and puts returned value into guess.\n\t\tans=check(guess,code);//sends guess and randomized code into check function, places return into ans (if answer is right then ans=1)\n\t\tround--;//decrease 1 from round each time, so the loop will repeat \'round\' times.\n\t\telapsedRound++;//stores the number of rounds elapsed by now.\t\n\t}\n\twhile(round!=0&&ans==0);//if user is out of rounds or if ans returns true, loop ends.\n\tif(ans==1)//checks if loop ended because user is out of rounds or because he guessed right and ans is true.\n\t{\n\t\tprintf(\"\\nYou won!!!\\n\");\n\t\tprintf(\"It took you only %d guesses, you are a professional code breaker!\\n\",elapsedRound);\n\t}\n\telse\n\t{\n\t\tprintf(\"\\nOOOOHHHH!!! Pancratius won and bought all of Hanukkah\'s gifts.\\n\");\n\t\tprintf(\"Nothing left for you...\\n\");\n\t\tprintf(\"The secret password was %d\\n\",code);\n\t}\t\n}\n/**\nGets 4 digits between 1-6 from the user. This is his guess.\n\nInput: none\nOutput: whole guess, as four-digit integer.\n*/\nint guessFunc()\n{\n\tint units, tens, hunds, thous, guess;\n\tprintf(\"\\nWrite your guess (only 1-6, no ENTER is needed)\\n\");\n\tdo\n\t{\n\t\tthous=getch()-48;\n\t}\n\twhile(thous<1||thous>6);//repeats if input is not between 1-6. leftmost digit.\n\tprintf(\"%d\",thous);//only prints after loop - if input is between 1-6.\n\tdo\n\t{\n\t\thunds=getch()-48;\n\t}\n\twhile(hunds<1||hunds>6);//second to the left\n\tprintf(\"%d\",hunds);\n\tdo\n\t{\n\t\ttens=getch()-48;\n\t}\n\twhile(tens<1||tens>6);//second to the right\n\tprintf(\"%d\",tens);\n\tdo\n\t{\n\t\tunits=getch()-48;\n\t}\n\twhile(units<1||units>6);//rightmost\n\tprintf(\"%d\\n\",units);\n\tguess=units+tens*10+hunds*100+thous*1000;//connects digits into a 4-digit integer.\n\treturn guess;\n}\n/**\nChecks how many digits missed and how many hit,\nby comparing all digits between randomized code and the user\'s guess this turn.\n\nInput: randomized code from randCode, user\'s guess from guessFunc.\nOutput: ans, \'bool\' if the whole guess was true, else false.\n*/\nint check(int guess, int code)\n{\t\n\tint tempC;//temporary code: copies code\'s value so it wouldn\'t be changed.\n\tint digitG, digitC;//digit guess, digit code: the rightmost digit of current value of each.\n\tint miss=0, hit=0;//count hits and misses for each round\n\tint i, j;//for the for loops. symbolizes the place of the digit in each number (i-digitG, j-digitC)\n\tint ans=0;//bool, as mentioned above.\n\tfor(i=1;i<=4;i++)//repeats four times-once for each digit of the guess.\n\t{\n\t\tdigitG=guess%10;//sets rightmost digit of guess as digitG\n\t\ttempC=code;//copies code into tempC, so code would stay as is, and so it would always start from the same number in the current game.\n\t\tfor(j=1;j<=4;j++)//repeats four times-once for each digit of the random code.\n\t\t{\n\t\t\tdigitC=tempC%10;//sets rightmost digit of code as digitC\n\t\t\tif(digitC==digitG)//checks if current digits of code and guess equal. if true, then there\'s a miss or a hit.\n\t\t\t{\n\t\t\t\tif(i==j)//checks if the place of the digits is equal. if true, then it\'s a hit.\n\t\t\t\t{\n\t\t\t\t\thit++;\n\t\t\t\t}\n\t\t\t\telse//if place of the digits is not equal, then it\'s a miss.\n\t\t\t\t{\n\t\t\t\t\tmiss++;\n\t\t\t\t}\n\t\t\t}\n\t\t\ttempC/=10;//divides tempC by 10, to move on to the next digit (to the right) of the random code.\n\t\t}\n\t\tguess/=10;//divides guess by 10, to move on to the next digit (to the right) of the user\'s guess.\n\t}\n\tprintf(\"You got %*d HITS %*d MISSES.\\n\",5,hit,5,miss);//print number of hits and/or misses.\n\tif (hit==4)//if there are 4 hits, it means the user\'s guess is right and then ans is returned true, and the current session is finished.\n\t{\n\t\tans=1;\n\t}\n\treturn ans;\n}\n",
                                        /*7 - Static & global vars*/"#include <stdio.h>\n#define NUM 1234\n#define FLOT 3.14\nint num = 4321;\nfloat flot = 41.3;\nvoid foo(int a);\nvoid bar(float a);\nint main()\n{\n\tprintf(\"%d, %.2f\\n\", NUM, FLOT);\n\tint i = 0;\n\tfor(i=0;i<10;i++){foo(i);bar(i);}\n}\nvoid foo(int a)\n{\n\tstatic int c = 0;\n\tstatic float count = 1;\n\tc++;\n\tcount *= 3.14;\n\tprintf(\"%.2f\\n\", NUM*flot*a);\n\tprintf(\"c: %d, count: %.2f\\n\", c, count);\n}\nvoid bar(float a)\n{\n\tstatic char count = 'a';\n\tcount++;\n\tprintf(\"%.2f\\n\", num*FLOT*a);\n\tprintf(\"count: %c\\n\", count);\n}",
                                        /*8 - Sort & search array*/"#include<stdio.h>\n#include<stdlib.h>\n\n#define TRUE 1\n#define FALSE !TRUE\n#define NOT_FOUND -1\n\nvoid scanArr(int arr[]);\nint scanNum();\nvoid sortArr(int arr[]);\nvoid findNum(int num, int arr[]);\nint main()\n{ \n\tint arr[15];\n\tscanArr(arr);\n\tint num = scanNum();\n\tsortArr(arr);\n\tfindNum(num, arr);\n\tsystem(\"PAUSE\");\n\treturn(0) ;\n} \n/**\n\tScans numbers for array\n\t\n\tInputs:\n\t\t\tarr[] - copies address of array.\n\tOutputs:\n\t\t\tnone\n**/\nvoid scanArr(int arr[])\n{\n\tint i;//index\n\tprintf(\"Enter 15 numbers for array:\\n\");\n\tfor(i=0;i<15;i++)\n\t{\n\t\tscanf(\"%d\",&arr[i]);\n\t\t_flushall();\n\t}\n}\n/**\n\tScans a number\n\t\n\tInputs:\n\t\t\tnone\n\tOutputs:\n\t\t\tnum - single scanned number.\n**/\nint scanNum()\n{\n\tint num;\n\tprintf(\"\\nEnter number to find in array: \");\n\tscanf(\"%d\",&num);\n\treturn num;\n}\n/**\n\tSorts scanned array (Bubble method)\n\t\n\tInputs:\n\t\t\tarr[] - the array.\n\tOutputs:\n\t\t\tnone\n**/\nvoid sortArr(int arr[])\n{\n\tint i,j;\n\tfor (i = 0; i < 14;i++)\n\t{\n\t\tfor (j = 0; j < 14 - i;j++)\n\t\t{\n\t\t\tif (arr[j] > arr[j+1])\n\t\t\t{\n\t\t\t\tint temp = arr[j];\n\t\t\t\tarr[j] = arr[j+1];\n\t\t\t\tarr[j+1] = temp;\n\t\t\t}\n\t\t}\n\t}\n\tprintf(\"\\n\\nSorted array:\\n\");\n\tfor(i=0;i<15;i++)\n\t{\n\t\tprintf(\"%d \",arr[i]);\n\t}\n\tprintf(\"\\n\\n\");\n}\n/**\n\tFinds number in array (binary search)\n\t\n\tInputs:\n\t\t\tarr[] - Scanned array after sort.\n\t\t\tnum - number to be found in array.\n\tOutputs:\n\t\t\tnone\n**/\nvoid findNum(int num, int arr[])\n{\n\tint left = 0;\n\tint right = 14;\n\tint middle;\n\tint place = NOT_FOUND;\n\n\twhile (left <= right && place == NOT_FOUND)\n\t{\n\t\tmiddle = ((left + right) / 2);\n\t\tif (arr[middle] == num)\n\t\t{\n\t\t\tplace = middle;\n\t\t}\n\t\telse if (num < arr[middle])\n\t\t{\n\t\t\tright = middle - 1;\n\t\t}\n\t\telse \n\t\t{\n\t\t\tleft = middle + 1;\n\t\t}\n\t}\n\tint isFound = (place != NOT_FOUND) ? TRUE : FALSE;\n\tif(isFound)\n\t{\n\t\tprintf(\"Found in place: %d\\n\",place);\n\t}\n\telse\n\t{\n\t\tprintf(\"Not found.\\n\");\n\t}\n}\n",
                                        /*9 - MagicSquare*/"#include <stdio.h>\n#include <stdlib.h>\n#include <string.h>\n\n#define LEN 100\nint magsq(int mat[LEN][LEN], int row, int col);\n\nint main()\n{\n\tint row, col;\n\tint i, j;\n\t\n\tprintf(\"Enter number of rows: (Max: 100)\\n\");\n\tscanf(\"%d\",&row);\n\tprintf(\"\\n\");\n\t_flushall();\n\t\n\tprintf(\"Enter number of columns: (Max: 100)\\n\");\n\tscanf(\"%d\",&col);\n\tprintf(\"\\n\");\n\t_flushall();\n\t\n\tprintf(\"Enter %d ints for matrix:\\n\",row*col);\n\tint mat[LEN][LEN];\n\tfor (i = 0; i < row; i++)\n\t{\n\t\tfor (j = 0; j < col; j++)\n\t\t{\n\t\t\tprintf(\"[%d][%d]: \",i,j);\n\t\t\tscanf(\"%d\",&mat[i][j]);\n\t\t\t_flushall();\n\t\t}\n\t}\n\t\n\tint flag=magsq(mat,row,col);\n\tif(flag==1)\n\t{\n\t\tprintf(\"It's a magic square!\\n\");\n\t}\n\telse\n\t{\n\t\tprintf(\"It's not a magic square...\\n\");\n\t}\n\t\n\tsystem(\"PAUSE\");\n\treturn 0;\n}\n/**\n\tChecks if the matrix is a magic square.\n\t\n\tInput:\n\t\tmatrix\n\t\t\n\tOutput:\n\t\tflag\n**/\nint magsq(int mat[LEN][LEN], int row, int col)\n{\n\tint sumRow[LEN]={0};\n\tint sumCol[LEN]={0};\n\tint sumDiag1=0;\n\tint sumDiag2=0;\n\tint i, j;\n\tint flag=1;\n\n\tif(row==col)\n\t{\n\t\tfor(i=0;i<row;i++)\n\t\t{\n\t\t\tfor(j=0;j<col;j++)\n\t\t\t{\n\t\t\t\tsumRow[i]+=mat[i][j];\n\t\t\t\tsumCol[j]+=mat[i][j];\n\t\t\t\t\n\t\t\t\tif(i==j)\n\t\t\t\t{\n\t\t\t\t\tsumDiag1+=mat[i][j];\n\t\t\t\t}\n\t\t\t\t\n\t\t\t\tif(j==(col-1)-i)\n\t\t\t\t{\n\t\t\t\t\tsumDiag2+=mat[i][j];\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t\t\n\t\tif (sumDiag1!=sumDiag2)\n\t\t{\n\t\t\tflag=0;\n\t\t}\n\t\t\n\t\tfor(i=0;i<row-1;i++)\n\t\t{\n\t\t\tif(sumRow[i]!=sumRow[i+1])\n\t\t\t{\n\t\t\t\tflag=0;\n\t\t\t}\n\t\t\t\n\t\t\tif(sumCol[i]!=sumCol[i+1])\n\t\t\t{\n\t\t\t\tflag=0;\n\t\t\t}\n\t\t}\n\t\t\n\t\tif(sumRow[0]!=sumCol[0])\n\t\t{\n\t\t\tflag=0;\n\t\t}\n\t\t\n\t\tif(sumRow[0]!=sumDiag1)\n\t\t{\n\t\t\tflag=0;\n\t\t}\n\t}\n\telse\n\t{\n\t\tflag=0;\n\t}\n\t\n\treturn flag;\n}\n",
                                        /*10 - SwapFreqLettersInStr*/"#include<stdio.h>\n#include<stdlib.h>\n#include<string.h>\nint main()\n{ \n\tprintf(\"Enter a string: (lower case)\\n\");\n\tchar str[100];\n\tgets(str);\n\tint l=strlen(str);\n\tchar ltrs[27]={0};\n\tint i, temp;\n\tfor(i=0;i<l;i++)\n\t{\n\t\tltrs[str[i]-'a']++;\n\t}\n\tint big1=0, big2=0;\n\tfor(i=0;i<26;i++)\n\t{\n\t\tif(ltrs[i]>ltrs[big1]||ltrs[big1]==ltrs[big2])\n\t\t{\n\t\t\tbig1=i;\n\t\t\tif(ltrs[big1]>ltrs[big2])\n\t\t\t{\n\t\t\t\ttemp=big2;\n\t\t\t\tbig2=big1;\n\t\t\t\tbig1=temp;\n\t\t\t}\n\t\t}\n\t}\n\tfor(i=0;i<l;i++)\n\t{\n\t\tif(str[i]==big1+'a')\n\t\t{\n\t\t\tstr[i]=big2+'a';\n\t\t}\n\t\telse if(str[i]==big2+'a')\n\t\t{\n\t\t\tstr[i]=big1+'a';\n\t\t}\n\t}\n\tputs(str);\n\tsystem(\"PAUSE\");\n\treturn(0) ;\n} \n"
        };
    }
}