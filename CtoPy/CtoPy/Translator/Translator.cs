﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CtoPy
{
    class Translator
    {
        public CCode Src { get; set; }
        public string Dest { get; private set; }
        private List<string> strApos;
        private List<string> charApos;
        private List<string> prefix;
        private List<string> suffix;
        /// <summary>
        /// bool: true if constant (define)
        /// </summary>
        private Dictionary<string, bool> globals;

        private Translator(CCode src)
        {
            Src = src;
            Dest = "";
            prefix = new List<string>();
            suffix = new List<string>();
            globals = new Dictionary<string, bool>();
        }
        public Translator(CCode src, List<string> strApos, List<string> charApos):this(src)
        {
            this.strApos = strApos;
            this.charApos = charApos;
        }

        public void Translate()
        {
            Translate(true);
        }
        private void Translate(bool wholeCode)
        {
            Translator inner;
            Line[] lines;
            Block[] blocks;
            if (wholeCode)
            {
                CCode precd = new CCode(CodeReplace(Src.Code, true));
                lines = precd.OuterLines;
                blocks = precd.Blocks;
            }
            else
            {
                lines = Src.OuterLines;
                blocks = Src.Blocks;
            }
            Block tBlk;
            int lineCount = 0, blockCount = 0;
            string temp;
            while (lineCount < lines.Length || blockCount < blocks.Length)
            {
                if (lineCount < lines.Length && (blockCount >= blocks.Length || lines[lineCount].Number < blocks[blockCount].Opener.Number))
                {
                    temp = LineReplace(ref lines[lineCount++]);
                    if (temp != "")
                        temp += "\n";
                    Dest += temp;
                }
                else
                {
                    inner = new Translator(new CCode(blocks[blockCount]));
                    inner.Translate(false);
                    blocks[blockCount] = new Block(inner.Src);
                    prefix = prefix.Union(inner.prefix).ToList();
                    suffix = suffix.Union(inner.suffix).ToList();
                    tBlk = blocks[blockCount];
                    tBlk.Code = inner.Dest;
                    temp = BlockReplace(ref blocks[blockCount++]/*, tBlk.ToString()*/);
                    if (temp != "")
                        temp += "\n";
                    Dest += temp;
                }
            }
            if (wholeCode)
            {
                if (globals.Count > 0)
                    GlobalDecl();
                if (Dest.Contains("\\s"))
                    StaticDecl();
                Dest = CodeReplace(Dest, false);
                Dest = string.Join("\n", prefix) + "\n" + Dest + "\n" + string.Join("\n", suffix);
                AposReplace();
            }
            Src.SetText(blocks, lines);
        }

        /// <summary>
        /// Replaces elements of ParseType "Code"
        /// </summary>
        /// <returns>Whether replacing was successful</returns>
        private string CodeReplace(string code, bool precode)
        {
            Dictionary<int, string> dic;
            if (precode)
                dic = CtoPyService.GetCsByParseType("PreCode");
            else
                dic = CtoPyService.GetCsByParseType("Code");
            string py, temp;
            int idx, jdx;
            List<string> sTmp, pTmp;
            foreach (var pair in dic)
            {
                sTmp = new List<string>();
                pTmp = new List<string>();
                py = CtoPyService.GetPythonByID(pair.Key);
                idx = py.IndexOfAny("$^".ToCharArray());
                while (idx != -1)
                {
                    temp = py.Substring(idx + 1);
                    jdx = temp.IndexOfAny("$^".ToCharArray());
                    if (!char.IsDigit(py[idx + 1]))
                    {
                        if (jdx != -1)
                            temp = temp.Substring(0, jdx);
                        if (py[idx] == '$')
                        {
                            if (!suffix.Contains(temp))
                                sTmp.Add(temp);
                        }
                        else
                        {
                            if (!prefix.Contains(temp))
                                pTmp.Add(temp);
                        }
                        py = py.Remove(idx, temp.Length + 1);
                        idx = jdx == -1 ? jdx : idx;
                    }
                    else
                        idx = jdx == -1 ? jdx : idx + jdx + 1;
                }
                switch (pair.Value)
                {
                    case "^":
                        if (py != "")
                            prefix.Add(py);
                        break;
                    case "$":
                        if (py != "")
                            suffix.Add(py);
                        break;
                    default:
                        if (Regex.IsMatch(code, pair.Value))
                        {
                            code = Regex.Replace(code, pair.Value, py);
                            prefix.AddRange(pTmp);
                            suffix.AddRange(sTmp);
                        }
                        break;
                }
            }
            return code;
        }
        /// <summary>
        /// Replaces element of ParseType "Line"
        /// </summary>
        /// <param name="lineidx">Index of line in code</param>
        /// <param name="formatid">Format row ID</param>
        /// <returns>Whether replacing was successful</return>
        private string LineReplace(ref Line ln)
        {
            Parser prs = new Parser();
            string replace, temp;
            string line = ln.ToString();
            int idx, tillCount = 0, orCount = 0;
            List<KeyValuePair<int, string>> vls;
            int[] keyCount;
            int[] maxCount;
            if (prs.ParseLine(line))
            {
                ln.SetLineType(CtoPyService.GetDescriptionByID(prs.ParsedID));
                vls = prs.FetchedValues;
                replace = CtoPyService.GetPythonByID(prs.ParsedID);
                keyCount = new int[GetMaxKey(vls) + 1];
                maxCount = GetKeyRepeats(vls);
                for (int j = 0; j < replace.Length; j++)
                {
                    if (replace[j] == '~')
                    {
                        replace = CopyTill(replace, j--, prs.RepeatAreasRepeats[tillCount]);
                        if (++tillCount == prs.RepeatAreasRepeats.Count)
                            tillCount = 0;
                    }
                    else if (replace[j] == '|')
                    {
                        replace = RemoveOr(replace, j--, prs.OptionalAreasExist[orCount]);
                        if (++orCount == prs.OptionalAreasExist.Count)
                            orCount = 0;
                    }
                    else if (replace[j] == '@' && j < replace.Length - 1)
                    {
                        if (Int32.TryParse(replace[j + 1].ToString(), out int num))
                        {
                            idx = GetKeyAtIdx(vls, num, keyCount[num]++);
                            replace = replace.Remove(j, 2);
                            replace = replace.Insert(j, vls[idx].Value);
                            j += vls[idx].Value.Length - 1;
                            if (keyCount[num] == maxCount[num])
                                keyCount[num] = 0;
                        }
                    }
                    else if (replace[j] == '$')
                    {
                        temp = replace.Substring(j + 1);
                        idx = temp.IndexOfAny("$^".ToCharArray());
                        if (idx != -1)
                            temp = temp.Substring(0, idx);
                        if (!suffix.Contains(temp))
                            suffix.Add(temp);
                        replace = replace.Remove(j--, temp.Length + 1);
                    }
                    else if (replace[j] == '^')
                    {
                        temp = replace.Substring(j + 1);
                        idx = temp.IndexOfAny("$^".ToCharArray());
                        if (idx != -1)
                            temp = temp.Substring(0, idx);
                        if (!prefix.Contains(temp))
                            prefix.Add(temp);
                        replace = replace.Remove(j--, temp.Length + 1);
                    }
                    else if (j + 2 < replace.Length && replace.Substring(j, 2) == "\\v")
                    {
                        if (Int32.TryParse(replace[j + 2].ToString(), out int num))
                        {
                            if (ln.Type == LineType.Define)
                                globals.Add(vls[GetKeyAtIdx(vls, num, 0)].Value, true);
                            else if (ln.Type == LineType.VarDecl && ln.Level == 0)
                                globals.Add(vls[GetKeyAtIdx(vls, num, 0)].Value, false);
                        }
                        replace = replace.Remove(j--, 3);
                    }
                }
                replace = Regex.Replace(replace, @"\n[\n\t]*\n", "\n");
                replace = Regex.Replace(replace, @"\n$", "");
                return replace;
            }
            ln.Type = LineType.Other;
            return line;
        }
        /// <summary>
        /// Replaces element of ParseType "Block"
        /// </summary>
        /// <param name="blockidx">Index of block in code</param>
        /// <param name="formatid">Format row ID</param>
        /// <returns>Whether replacing was successful</return>
        private string BlockReplace(ref Block src/*, string block*/)
        {
            Parser prs = new Parser();
            string replace, temp;
            int idx, tillCount = 0, orCount = 0;
            List<KeyValuePair<int, string>> vls;
            int[] keyCount;
            int[] maxCount;
            if (prs.ParseBlock(/*block*/src.ToString()))
            {
                src.SetBlockType(CtoPyService.GetDescriptionByID(prs.ParsedID));
                vls = prs.FetchedValues;
                replace = CtoPyService.GetPythonByID(prs.ParsedID);
                keyCount = new int[GetMaxKey(vls) + 1];
                maxCount = GetKeyRepeats(vls);
                for (int j = 0; j < replace.Length; j++)
                {
                    if (replace[j] == '~')
                    {
                        replace = CopyTill(replace, j, prs.RepeatAreasRepeats[tillCount++]);
                        if (tillCount == prs.RepeatAreasRepeats.Count)
                            tillCount = 0;
                    }
                    else if (replace[j] == '|')
                    {
                        replace = RemoveOr(replace, j, prs.OptionalAreasExist[orCount++]);
                        if (orCount == prs.OptionalAreasExist.Count)
                            orCount = 0;
                    }
                    else if (replace[j] == '@' && j < replace.Length - 1)
                    {
                        if (Int32.TryParse(replace[j + 1].ToString(), out int num))
                        {
                            idx = GetKeyAtIdx(vls, num, keyCount[num]++);
                            replace = replace.Remove(j, 2);
                            replace = replace.Insert(j, vls[idx].Value);
                            j += vls[idx].Value.Length - 1;
                            if (keyCount[num] == maxCount[num])
                                keyCount[num] = 0;
                        }
                    }
                    else if (replace[j] == '$')
                    {
                        temp = replace.Substring(j);
                        if (!suffix.Contains(temp))
                        {
                            suffix.Add(temp);
                            replace.Remove(j);
                        }
                    }
                    else if (replace[j] == '^')
                    {
                        temp = replace.Substring(j);
                        if (!prefix.Contains(temp))
                        {
                            prefix.Add(temp);
                            replace.Remove(j);
                        }
                    }
                }
                replace = Regex.Replace(replace, @"\n[\n\t]*\n", "\n");
                replace = Regex.Replace(replace, @"\n$", "");
                return replace;
            }
            else return src.ToString();
        }
        /// <summary>
        /// Return extracted apos to their locations
        /// </summary>
        private void AposReplace()
        {
            string num = "";
            string rep = "";
            bool inQt = false;
            for (int i = 0; i < Dest.Length; i++)
            {
                if (Dest[i] == '"')
                    inQt = !inQt;
                else if ("$@".Contains(Dest[i]))
                {
                    num = ExtractNum(Dest, i + 1);
                    if (Dest[i] == '$')
                        rep = strApos[int.Parse(num)];
                    else if (inQt)
                        rep = charApos[int.Parse(num)].Trim('\'');
                    else
                        //rep = "ord(" + charApos[int.Parse(num)] + ")";
                        rep = CtoPyService.GetPropByDescription("Char").Replace("@", charApos[int.Parse(num)]);
                    Dest = Dest.Remove(i, num.Length + 1);
                    Dest = Dest.Insert(i, rep);
                    i += rep.Length - 1;
                }
            }
        }
        /// <summary>
        /// Declare global variables in the beginning of functions that use them
        /// </summary>
        private void GlobalDecl()
        {
            string dst, pattern;
            string[] foos;
            int idx, len, i;
            MatchCollection defs = Regex.Matches(Dest, CtoPyService.GetPropByDescription("FuncDefRgx"));
            if (defs.Count > 0)
            {
                foos = new string[defs.Count];
                dst = Dest.Substring(0, defs[0].Index);
                for (i = 0; i < foos.Length - 1; i++)
                {
                    idx = defs[i].Index;
                    len = defs[i + 1].Index - idx;
                    foos[i] = Dest.Substring(idx, len);
                }
                foos[i] = Dest.Substring(defs[i].Index);

                for (i = 0; i < foos.Length; i++)
                {
                    foreach (var glob in globals)
                    {
                        idx = foos[i].IndexOf('\n') + 1;
                        pattern = @"(?<=\W)" + glob.Key + @"(?=\W)";
                        if (Regex.IsMatch(foos[i], pattern))
                            if (!glob.Value)
                                foos[i] = foos[i].Insert(idx, CtoPyService.GetPropByDescription("GlobalDecl").Replace("@", glob.Key));
                    }
                    dst += foos[i];
                }
                foreach (var glob in globals)
                {
                    if (glob.Value)
                    {
                        pattern = @"(?<!=\w)" + glob.Key + @"(?!=\w)";
                        dst = Regex.Replace(dst, pattern, CtoPyService.GetPropByDescription("Const").Replace("@", glob.Key));
                    }
                }
                Dest = dst;
            }
        }
        /// <summary>
        /// Declare static variables in functions
        /// </summary>
        private void StaticDecl()
        {
            string dst, line, name, val, fooname, tmp;
            string[] foos;
            int idx, len, i;
            MatchCollection defs = Regex.Matches(Dest, CtoPyService.GetPropByDescription("FuncDefRgx"));
            if (defs.Count > 0)
            {
                foos = new string[defs.Count];
                dst = Dest.Substring(0, defs[0].Index);
                for (i = 0; i < foos.Length - 1; i++)
                {
                    idx = defs[i].Index;
                    len = defs[i + 1].Index - idx;
                    foos[i] = Dest.Substring(idx, len);
                }
                foos[i] = Dest.Substring(defs[i].Index);

                for (i = 0; i < foos.Length; i++)
                {
                    idx = foos[i].IndexOf("\\s");
                    fooname = Regex.Match(foos[i], CtoPyService.GetPropByDescription("FooName")).Groups[1].Value;
                    while (idx != -1)
                    {
                        line = foos[i].Substring(idx + 2, foos[i].IndexOf('\n', idx) - idx - 2);
                        name = Regex.Match(line, CtoPyService.GetPropByDescription("StatName")).Groups[1].Value;
                        val = Regex.Match(line, CtoPyService.GetPropByDescription("StatVal")).Groups[1].Value;
                        foos[i] = foos[i].Remove(idx, line.Length + 4);
                        tmp = CtoPyService.GetPropByDescription("Static").Replace("!", fooname).Replace("@", name);
                        foos[i] = Regex.Replace(foos[i], @"(?<=\W)" + name + @"(?=\W)", tmp);
                        tmp = CtoPyService.GetPropByDescription("PreStat").Replace("!", fooname).Replace("@", name).Replace("#", val);
                        foos[i] += tmp;
                        idx = foos[i].IndexOf("\\s");
                    }
                    dst += foos[i];
                }
                Dest = dst;
            }
        }

        /// <summary>
        /// Gets the number of repeats of each key
        /// </summary>
        /// <param name="vls">List of parameters</param>
        /// <returns>Array of key repeats</returns>
        private int[] GetKeyRepeats(List<KeyValuePair<int, string>> vls)
        {
            int[] counts = new int[GetMaxKey(vls) + 1];
            for (int i = 0; i < vls.Count; i++)
                counts[vls[i].Key]++;
            return counts;
        }
        /// <summary>
        /// Gets the key with the highest value
        /// </summary>
        /// <param name="vls">List of parameters</param>
        /// <returns>Highest key value</returns>
        private int GetMaxKey(List<KeyValuePair<int, string>> vls)
        {
            int max = 0;
            foreach (var item in vls)
                max = Math.Max(max, item.Key);
            return max;
        }
        /// <summary>
        /// Get the first item in the list that its key is num.
        /// </summary>
        /// <param name="vls"> List to search from</param>
        /// <param name="num">Key to search for</param>
        /// <returns>Index of item with requiered key</returns>
        private int GetKeyAtIdx(List<KeyValuePair<int, string>> vls ,int num, int idx)
        {
            int count = 0;
            for(int i = 0; i < vls.Count; i ++)
                if (vls[i].Key == num)
                    if (count == idx)
                        return i;
                    else
                        count++;
            return -1;
        }
        /// <summary>
        /// Function that takes care of repitions according to the ~ operator and the db.
        /// </summary>
        /// <param name="str">string to edit.</param>
        /// <param name="idx">index of said ~ operator</param>
        /// <param name="num">number of repitions</param>
        /// <returns>Changed string in text</returns>
        private string CopyTill(string str, int idx, int num)
        {
            int EndTill = str.IndexOf('~', idx + 1);
            string subStr = str.Substring(idx + 1, EndTill - idx - 1);
            str = str.Remove(idx, EndTill - idx + 1);
            int i;
            for (i = 0; i < num; i++)
                str = str.Insert(idx + i * subStr.Length, subStr);
            if (subStr.EndsWith(","))
                str = str.Remove(idx + i * subStr.Length - 1, 1);
            else if(subStr.EndsWith(", "))
                str = str.Remove(idx + i * subStr.Length - 2, 2);
            return str;
        }
        /// <summary>
        /// Removes the | operator(used in DB) and it's section.
        /// </summary>
        /// <param name="str">String to remove from</param>
        /// <param name="idx">index of | operator</param>
        /// <returns>changed string</returns>
        private string RemoveOr(string str, int idx, bool keep)
        {           
            int EndOr = str.IndexOf('|', idx + 1);
            string subStr = str.Substring(idx + 1, EndOr - idx - 1);
            str = str.Remove(idx, subStr.Length + 2);
            if(keep)
                str = str.Insert(idx, subStr);
            return str;
        }
        /// <summary>
        /// Extract a number from a string
        /// </summary>
        /// <param name="str">String to work on</param>
        /// <param name="start">Starting index</param>
        /// <returns>The number extracted</returns>
        private string ExtractNum(string str, int start)
        {
            string ret = "";
            for (int i = start; i < str.Length && GlobalConstants.Digits.Contains(str[i]); i++)
                ret += str[i];
            return ret;
        }
    }
}
