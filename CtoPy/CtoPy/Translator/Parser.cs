﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CtoPy
{
    class Parser
    {
        /// <summary>
        /// Contains whether each optional area in format is used in given code
        /// </summary>
        public List<bool> OptionalAreasExist { get; private set; }
        /// <summary>
        /// Contains number of repeats in code for each repeatable area in format
        /// </summary>
        public List<int> RepeatAreasRepeats { get; private set; }
        /// <summary>
        /// Contains the values parsed from text with format
        /// </summary>
        public List<KeyValuePair<int, string>> FetchedValues { get; private set; }
        /// <summary>
        /// ID in database of parsed format
        /// </summary>
        public int ParsedID { get; private set; }
        ///// <summary>
        ///// Type of parsed block/line
        ///// </summary>
        //public Enum Type { get; private set; }
        /// <summary>
        /// Length of parsed text for recursive use of Parse
        /// </summary>
        private int RecurParseLength;

        public Parser()
        {
            Init();
        }

        /// <summary>
        /// Parses a line of code
        /// </summary>
        /// <param name="line">The line of code</param>
        /// <returns>Whether parsing was successful</returns>
        public bool ParseLine(string line)
        {
            foreach (var format in CtoPyService.GetCsByParseType("Line"))
            {
                Init();
                if (Parse(line, format.Value))
                {
                    ParsedID = format.Key;
                    return true;
                }
            }
            Init();
            return false;
        }
        /// <summary>
        /// Parses a block of code
        /// </summary>
        /// <param name="block">The code block</param>
        /// <returns>Whether parsing was successful</returns>
        public bool ParseBlock(string block)
        {
            foreach (var format in CtoPyService.GetCsByParseType("Block"))
            {
                Init();
                if (Parse(block, format.Value))
                {
                    ParsedID = format.Key;
                    return true;
                }
            }
            Init();
            return false;
        }

        /// <summary>
        /// Parses given text with format
        /// </summary>
        /// <param name="text">Text to parse</param>
        /// <param name="format">Format to parse with</param>
        /// <returns>Whether parsing was successful</returns>
        private bool Parse(string text, string format)
        {
            return Parse(text, format, false);
        }
        private bool Parse(string text, string format, bool inner)
        {
            try
            {
                int txtCnt = 0;
                char chAfter = '\0';
                int endIdx = 0;
                string tStr = "", tStr2 = "";
                int tInt = 0;
                bool tBool = false, comma, opt, skip;
                bool retVal = true;
                for (int i = 0; i < format.Length && retVal; i++)
                {
                    if (txtCnt >= text.Length)
                        return false;
                    if (format[i] == '@')
                    {
                        comma = false;
                        opt = false;
                        skip = false;
                        if (i++ + 1 < format.Length)
                            chAfter = format.Substring(i + 1).FirstOrDefault(c => c != '\n' && c != '\t' && c != ' ');
                        else chAfter = '\0';
                        if (chAfter == '|')
                        {
                            tInt = format.IndexOf('|', i);
                            endIdx = GetEndIdx(format, '|', tInt);
                            chAfter = format[endIdx + 1];
                            opt = true;
                        }
                        else if (chAfter == '>')
                        {
                            chAfter = format[i + 3];
                            skip = true;
                        }
                        if (chAfter == '_')
                            chAfter = ' ';
                        else if (inner && chAfter == ',')
                        {
                            if (!text.Substring(txtCnt, GetEndBrack(text, txtCnt) - txtCnt).Contains(','))
                            {
                                chAfter = format[format.IndexOf(chAfter) + 1];
                                comma = true;
                            }
                        }
                        tStr = GetPar(text, txtCnt, chAfter).Trim(' ');
                        txtCnt += tStr.Length;
                        if (opt)
                        {
                            tStr2 = format.Substring(tInt + 1, endIdx - tInt - 1);
                            tBool = tStr.EndsWith(tStr2);
                            OptionalAreasExist.Add(tBool);
                            if (tBool)
                                tStr = tStr.Remove(tStr.Length - tStr2.Length);
                            format = format.Remove(tInt, tStr2.Length + 2);
                        }
                        else if (skip)
                            if (tStr.Contains(format[i + 2]))
                                tStr = tStr.Remove(tStr.IndexOf(format[i + 2]));
                        FetchedValues.Add(new KeyValuePair<int, string>(format[i] - '0', tStr));
                        if (comma)
                        {
                            i += 2;
                            comma = false;
                        }
                        if (skip)
                        {
                            i += 2;
                        }
                    }
                    else if (format[i] == '~')
                    {
                        tInt = 0;
                        endIdx = GetEndIdx(format, '~', i);
                        while (Parse(text.Substring(txtCnt), format.Substring(i + 1, endIdx - i - 1), true))
                        {
                            tInt++;
                            txtCnt += RecurParseLength;
                        }
                        RepeatAreasRepeats.Add(tInt);
                        i = endIdx;
                    }
                    else if (format[i] == '\\')
                    {
                        if (i + 1 >= format.Length || format[i + 1] != text[txtCnt])
                            return false;
                    }
                    else if (format[i] == '|')
                    {
                        tInt = 0;
                        endIdx = GetEndIdx(format, '|', i);
                        tBool = Parse(text.Substring(txtCnt), format.Substring(i + 1, endIdx - i - 1), true);
                        OptionalAreasExist.Add(tBool);
                        i = endIdx;
                        if (tBool)
                            txtCnt += RecurParseLength;
                    }
                    else if (format[i] == '[' && i + 1 < format.Length && format[i + 1] == '^')
                    {
                        tInt = 0;
                        if (i + 2 >= format.Length || format[i + 2] != text[txtCnt])
                            retVal = false;
                        i += 3;
                        txtCnt++;
                    }
                    else if ((format[i] != text[txtCnt]) && !(format[i] == '_' && text[txtCnt] == ' '))
                        retVal = false;
                    else if (inner && format[i] == ',')
                    {
                        txtCnt++;
                        i++;
                    }
                    else txtCnt++;
                }
                if (inner && txtCnt == 0)
                    retVal = false;
                RecurParseLength = txtCnt;
                return retVal;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the parameter (@)
        /// </summary>
        /// <param name="text">Code to get parameter from</param>
        /// <param name="from">Index after parameter identifier (@)</param>
        /// <param name="to">Char to find</param>
        /// <returns>The found parameter</returns>
        private string GetPar(string text, int from, char to)
        {
            int reopens = 0;
            char opener = '\0';
            int idx = 0;
            switch (to)
            {
                case '}':
                    opener = '{';
                    break;
                case ')':
                    opener = '(';
                    break;
                case ']':
                    opener = '[';
                    break;
            }
            for (idx = from; idx < text.Length; idx++)
            {
                if (text[idx] == opener)
                    reopens++;
                else if (text[idx] == to)
                    if (reopens > 0)
                        reopens--;
                    else break;
            }
            if (to != '\0')
                return text.Substring(from, idx - from);
            else return text.Substring(from);
        }
        /// <summary>
        /// Find the closing bracket ')'
        /// </summary>
        /// <param name="text">Text to search</param>
        /// <param name="from">Where to statr looking</param>
        /// <returns></returns>
        private int GetEndBrack(string text, int from)
        {
            int reopens = 0;
            int idx;
            for (idx = from; idx < text.Length; idx++)
            {
                if (text[idx] == '(')
                    reopens++;
                else if (text[idx] == ')')
                    if (reopens > 0)
                        reopens--;
                    else break;
            }
            return idx;
        }
        /// <summary>
        /// Gets index of matching closer (chiefly for ~ and |)
        /// </summary>
        /// <param name="format">Format to search closer on</param>
        /// <param name="find">Char to find</param>
        /// <param name="from">Index after opener</param>
        /// <returns>Index of closer</returns>
        private int GetEndIdx(string format, char find, int from)
        {
            return format.IndexOf(find, from + 1);
        }
        /// <summary>
        /// Initializes lists
        /// </summary>
        private void Init()
        {
            FetchedValues = new List<KeyValuePair<int, string>>();
            RepeatAreasRepeats = new List<int>();
            OptionalAreasExist = new List<bool>();
            ParsedID = 0;
        }
    }
}
